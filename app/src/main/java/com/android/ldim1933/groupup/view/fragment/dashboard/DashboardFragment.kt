package com.android.ldim1933.groupup.view.fragment.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.TaskStateEnum
import com.android.ldim1933.groupup.data.model.NotificationData
import com.android.ldim1933.groupup.data.model.Poll
import com.android.ldim1933.groupup.data.model.Task
import com.android.ldim1933.groupup.databinding.FragmentDashboardBinding
import com.android.ldim1933.groupup.view.adapter.DashboardViewPagerAdapter
import com.android.ldim1933.groupup.viewmodel.NotificationViewModel
import com.android.ldim1933.groupup.viewmodel.PollRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.TasksViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import me.relex.circleindicator.CircleIndicator3

class DashboardFragment : Fragment() {

    private lateinit var binding: FragmentDashboardBinding
    private lateinit var taskViewModel: TasksViewModel
    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var pollRepositoryViewModel: PollRepositoryViewModel
    private var titleList = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        taskViewModel = ViewModelProvider(this)[TasksViewModel::class.java]
        notificationViewModel = ViewModelProvider(this)[NotificationViewModel::class.java]
        pollRepositoryViewModel = ViewModelProvider(this)[PollRepositoryViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        binding.viewPagerDashboard.orientation = ViewPager2.ORIENTATION_VERTICAL
        postToList()

        binding.dashboardLayout.setOnRefreshListener {
            val fragment = DashboardFragment()
            fragmentManager!!.beginTransaction().replace(this.id, fragment).commit()
            binding.dashboardLayout.isRefreshing = false
        }


        binding.dashboardProgressBar.visibility = View.VISIBLE
        val dashboardAdapter = DashboardViewPagerAdapter(
            titleList,
            emptyArray<Task>().toMutableList(),
            taskViewModel,
            emptyArray<NotificationData>().toMutableList(),
            emptyArray<Poll>().toMutableList()
        )
        binding.viewPagerDashboard.adapter = dashboardAdapter

        if (Firebase.auth.currentUser?.uid != null) {
            taskViewModel.getUserTasks(
                Firebase.auth.currentUser!!.uid,
                binding.dashboardProgressBar
            ).observe(this.viewLifecycleOwner, { tasks ->
                dashboardAdapter.addTodoList(tasks.filter { it.state == TaskStateEnum.PENDING.title } as MutableList<Task>)
                binding.viewPagerDashboard.adapter = dashboardAdapter
            })

            notificationViewModel.getUserNotifications(
                Firebase.auth.currentUser!!.uid,
                binding.dashboardProgressBar
            ).observe(this.viewLifecycleOwner, { notifications ->
                dashboardAdapter.addNotificationList(notifications as MutableList<NotificationData>)
                binding.viewPagerDashboard.adapter = dashboardAdapter
            })

            pollRepositoryViewModel.getUserPolls(
                Firebase.auth.currentUser!!.uid,
                binding.dashboardProgressBar
            ).observe(this.viewLifecycleOwner, { polls ->
                dashboardAdapter.addPollList(polls as MutableList<Poll>)
                binding.viewPagerDashboard.adapter = dashboardAdapter
            })

            val indicator: CircleIndicator3 = binding.circleIndicatorDashboard
            indicator.setViewPager(binding.viewPagerDashboard)
        }

        return binding.root
    }

    private fun addToList(title: String) {
        titleList.add(title)
    }

    private fun postToList() {
        addToList(getString(R.string.to_do))
        addToList(getString(R.string.reminders_for_me))
        addToList(getString(R.string.poll))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        titleList.clear()
    }

}