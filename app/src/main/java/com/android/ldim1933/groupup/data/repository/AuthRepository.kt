package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.content.Intent
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.view.MainActivity
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class AuthRepository(application: Application) {
    private var mApplication: Application = application
    var firebaseUserMutableLiveData = MutableLiveData<FirebaseUser>()
    var userLoggedMutableLiveData = MutableLiveData<Boolean>()
    private val mAuth = Firebase.auth

    init {
        if (mAuth.currentUser != null) {
            firebaseUserMutableLiveData.postValue(mAuth.currentUser)
        }
    }

    fun register(email: String, password: String) {
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    firebaseUserMutableLiveData.postValue(mAuth.currentUser)
                    Toast.makeText(
                        mApplication,
                        mApplication.getString(R.string.register_sucessfull),
                        Toast.LENGTH_LONG
                    ).show()

                } else {
                    Toast.makeText(
                        mApplication,
                        task.exception?.message.toString(),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }

    fun logIn(email: String, password: String) {
        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    firebaseUserMutableLiveData.postValue(mAuth.currentUser)
                    Toast.makeText(mApplication,   mApplication.getString(R.string.login_sucessfull), Toast.LENGTH_LONG)
                        .show()

                    val intent = Intent(mApplication, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("activityType", "login")
                    intent.putExtra("user", mAuth.currentUser)
                    startActivity(mApplication, intent, null)

                } else {
                    Toast.makeText(
                        mApplication,
                        mApplication.getString(R.string.login_invalid),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }

    fun signOut() {
        mAuth.signOut()
        userLoggedMutableLiveData.postValue(true)
    }


}