package com.android.ldim1933.groupup.view.fragment.group

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.databinding.FragmentGroupBinding
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class SpecificGroupFragment : Fragment() {

    private lateinit var binding: FragmentGroupBinding
    private val args: SpecificGroupFragmentArgs by navArgs()
    private lateinit var dialogLeave: Dialog
    private lateinit var dialogDelete: Dialog
    private lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var groupDataViewModel: GroupRepositoryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        groupDataViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]

        dialogLeave = createDialog(0)
        dialogDelete = createDialog(1)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGroupBinding.inflate(inflater, container, false)

        val group = args.group

        binding.groupNameTxt.text = group.name
        binding.groupDescriptionTxt.text = group.description
        if (group.profilePictureUrl != null) {
            Glide.with(this).load(group.profilePictureUrl)
                .into(binding.groupImage)
        }

        if (args.group.admins.contains(Firebase.auth.currentUser!!.uid)) {
            binding.deleteGroupBtn.visibility = View.VISIBLE
        } else {
            binding.deleteGroupBtn.visibility = View.GONE
        }

        binding.leaveGroupBtn.setOnClickListener {
            dialogLeave.show()
        }

        binding.deleteGroupBtn.setOnClickListener {
            dialogDelete.show()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.groupInfoButton.setOnClickListener {
            findNavController().navigate(
                SpecificGroupFragmentDirections.actionGroupFragmentToGroupInfoFragment(
                    args.group
                )
            )
        }

        binding.groupMembersButton.setOnClickListener {
            findNavController().navigate(
                SpecificGroupFragmentDirections.actionGroupFragmentToGroupMembersFragment(
                    args.group
                )
            )
        }

        binding.groupAdminsButton.setOnClickListener {
            findNavController().navigate(
                SpecificGroupFragmentDirections.actionGroupFragmentToGroupAdminsFragment(
                    args.group
                )
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun createDialog(type: Int): Dialog {
        val dialog = this.context?.let { Dialog(it) }!!
        dialog.setContentView(R.layout.invitation_dialog)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.attributes?.windowAnimations = R.style.animationDialog
        dialog.setCancelable(false)

        if (type == 0) {
            dialog.findViewById<Button>(R.id.btn_invite).text = "Leave"
            dialog.findViewById<TextView>(R.id.textViewInvite).text =
                "Do you want to leave this group?"
            dialog.findViewById<ImageView>(R.id.dialogImage).setImageResource(R.drawable.back_icon)
        } else {
            dialog.findViewById<Button>(R.id.btn_invite).text = "Delete"
            dialog.findViewById<TextView>(R.id.textViewInvite).text =
                "Do you want to delete this group?"
            dialog.findViewById<ImageView>(R.id.dialogImage)
                .setImageResource(R.drawable.delete_icon)
        }

        dialog.findViewById<Button>(R.id.btn_invite).setOnClickListener {
            if (type == 0) {
                var newAdmin: String? = null
                if (args.group.admins.size == 1 && args.group.admins.contains(Firebase.auth.currentUser!!.uid)) {
                    if (args.group.members.size == 1) {
                        groupDataViewModel.deleteGroup(args.group.id!!)
                    } else {
                        run lit@{
                            args.group.members.forEach {
                                if (it != Firebase.auth.currentUser!!.uid) {
                                    newAdmin = it
                                    return@lit
                                }
                            }
                        }
                        userDataViewModel.leaveGroup(
                            Firebase.auth.currentUser!!.uid,
                            args.group.id!!
                        )
                        newAdmin?.let { it1 ->
                            groupDataViewModel.addNewAdminAndRemoveAnother(
                                args.group.id!!,
                                it1, Firebase.auth.currentUser!!.uid
                            )
                        }
                    }
                } else {
                    userDataViewModel.leaveGroup(Firebase.auth.currentUser!!.uid, args.group.id!!)
                }

            } else {
                groupDataViewModel.deleteGroup(args.group.id!!)
            }
            this.activity!!.onBackPressed()
            dialog.dismiss()
        }

        dialog.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            dialog.dismiss()
        }
        return dialog
    }
}