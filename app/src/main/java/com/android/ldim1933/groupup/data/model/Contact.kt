package com.android.ldim1933.groupup.data.model

data class Contact(
    var name: String? = null,
    var image: String? = null,
    var telephoneNumber: String? = null,
    var hasApplication: Boolean = false,
    var userId: String? = null,
    var profileUrl: String? = null
)