package com.android.ldim1933.groupup.view.fragment.menu

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.databinding.FragmentGroupInvitationBinding
import com.android.ldim1933.groupup.view.adapter.GroupInvitationAdapter
import com.android.ldim1933.groupup.viewmodel.GroupInvitationViewModel
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class GroupInvitationFragment : Fragment() {

    private lateinit var binding: FragmentGroupInvitationBinding
    private lateinit var groupInvitationVewModel: GroupInvitationViewModel
    private lateinit var adapter: GroupInvitationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        groupInvitationVewModel = ViewModelProvider(this)[GroupInvitationViewModel::class.java]

    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGroupInvitationBinding.inflate(inflater, container, false)
        binding.noInvitationTxt.visibility = View.VISIBLE

        if (Firebase.auth.currentUser?.uid != null) {
            groupInvitationVewModel.getUserInvitation(Firebase.auth.currentUser?.uid!!)
                .observe(this, {
                    adapter = GroupInvitationAdapter(
                        it,
                        ViewModelProvider(this)[GroupRepositoryViewModel::class.java],
                        groupInvitationVewModel
                    )
                    if (it.size != 0) {
                        if (it.size > 99) {
                            binding.groupInvitationTextView.text =
                                "You have 99+ invitation to groups"
                        } else {
                            binding.groupInvitationTextView.text =
                                "You have ${it.size} invitation to groups"
                        }
                        binding.noInvitationTxt.visibility = View.GONE
                        binding.recyclerViewGroupInvitation.adapter = adapter
                    }
                })
        }

        return binding.root
    }
}