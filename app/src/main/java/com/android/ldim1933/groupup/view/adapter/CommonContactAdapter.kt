package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Contact
import java.util.*
import kotlin.collections.ArrayList


class CommonContactAdapter(private var list: ArrayList<Contact>) :
    RecyclerView.Adapter<CommonContactAdapter.ContactViewHolder>() {

    private var originalList = ArrayList<Contact>()
    private var filteredList = ArrayList<Contact>()
    private var currentList = ArrayList<Contact>()

    init {
        originalList.addAll(list)
        filteredList = list.filter { contact -> contact.hasApplication } as ArrayList<Contact>
        currentList.addAll(list)
    }

    inner class ContactViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.contact_item_name)
        var number: TextView = itemView.findViewById(R.id.contact_item_number)
        var imageTxt: TextView = itemView.findViewById(R.id.contact_item_image)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.contact_item_2, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact: Contact = list[position]
        holder.name.text = contact.name
        holder.number.text = contact.telephoneNumber

        val senderFirstLetter: String = holder.name.text.subSequence(0, 2) as String
        holder.imageTxt.text = senderFirstLetter
        holder.imageTxt.setBackgroundColor(getColor())
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun getColor(): Int {
        val randomBackgroundColor = Random()
        return Color.argb(
            255,
            randomBackgroundColor.nextInt(256),
            randomBackgroundColor.nextInt(256),
            randomBackgroundColor.nextInt(256)
        )
    }

    fun getContact(position: Int): Contact {
        return list[position]
    }

    fun setContactList(contactList: List<Contact>) {
        list.clear()
        list.addAll(contactList)
    }
}