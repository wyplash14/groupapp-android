package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Poll
import com.android.ldim1933.groupup.data.model.PollPoint
import com.android.ldim1933.groupup.data.model.User
import com.android.ldim1933.groupup.viewmodel.PollRepositoryViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class PollPointAdapter(
    private val list: MutableList<PollPoint>,
    private val poll: Poll,
    private val pollRepositoryViewModel: PollRepositoryViewModel,
    private val groupUsers: List<User>
) :
    RecyclerView.Adapter<PollPointAdapter.PollPointViewHolder>() {

    private var originalNotFilteredList: MutableList<PollPoint> = mutableListOf()

    init {
        originalNotFilteredList.addAll(list)
    }

    class PollPointViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val description: TextView = itemView.findViewById(R.id.vote_point_txt)
        val voters: Spinner = itemView.findViewById(R.id.scroll_voters)
        val progressBar: ProgressBar = itemView.findViewById(R.id.vote_progress_bar)
        val votersNumber: TextView = itemView.findViewById(R.id.voters_num_txt)
        val buttonVote: Button = itemView.findViewById(R.id.vote_button)
        val buttonUnVote: Button = itemView.findViewById(R.id.unvote_button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PollPointViewHolder {
        return PollPointViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.poll_point_item, parent, false)
        )
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: PollPointViewHolder, position: Int) {
        holder.description.text = list[position].description

        val usersVoted = arrayListOf<User>()
        groupUsers.forEach { user ->
            if (list[position].voters.contains(user.uid)) usersVoted.add(
                user
            )
        }
        holder.voters.adapter = UserSpinnerAdapter(usersVoted)
        holder.votersNumber.text = usersVoted.size.toString() + " " + holder.itemView.context.getString(R.string.vote)
        holder.progressBar.max = groupUsers.size
        holder.progressBar.progress = usersVoted.size

        holder.buttonVote.setOnClickListener {
            pollRepositoryViewModel.addVoteToPollPoint(
                poll,
                list[position],
                Firebase.auth.uid.toString()
            )
            list[position].voters.add(Firebase.auth.uid.toString())
            notifyDataSetChanged()

        }

        holder.buttonUnVote.setOnClickListener {
            pollRepositoryViewModel.removeVoteFromPollPoint(
                poll,
                list[position],
                Firebase.auth.uid.toString()
            )
            list[position].voters.remove(Firebase.auth.uid.toString())
            list[position].voters.remove(Firebase.auth.uid.toString())
            notifyDataSetChanged()
        }

        if (list[position].voters.contains(Firebase.auth.uid.toString())) {
            holder.buttonVote.visibility = View.GONE
            holder.buttonUnVote.visibility = View.VISIBLE
        } else {
            holder.buttonUnVote.visibility = View.GONE
            holder.buttonVote.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        list.clear()
    }

}