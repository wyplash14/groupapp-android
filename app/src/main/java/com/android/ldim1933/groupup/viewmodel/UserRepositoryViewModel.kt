package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.data.model.Contact
import com.android.ldim1933.groupup.data.model.User
import com.android.ldim1933.groupup.data.repository.UserRepository

class UserRepositoryViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = UserRepository(application)
    private val userData = repository.userData
    private val contactsHasApplication = MutableLiveData<ArrayList<Contact>>()
    private val userDataTelephoneList = repository.userDataTelephoneList

    fun create(user: User, imageUri: Uri?) {
        repository.create(user, imageUri)
    }

    fun getUserData(userId: String): MutableLiveData<User> {
        repository.getUserbyId(userId)
        return userData
    }

    fun getUserGroupsById(userId: String): MutableLiveData<ArrayList<String>> {
        val groupData = MutableLiveData<ArrayList<String>>()
        repository.loadGroupsOfUser(userId, groupData)
        return groupData
    }


    fun getContactHasApplicationByPhoneNumber(contactList: ArrayList<Contact>): MutableLiveData<ArrayList<Contact>> {
        contactsHasApplication.postValue(contactList)

        contactList.forEach { contact ->
            contact.telephoneNumber?.let { telephoneNumber ->
                repository.getUserByTelephoneNumber(telephoneNumber)
            }
        }

        userDataTelephoneList.observeForever { userDataList ->
            contactList.forEach { contact ->
                userDataList.forEach { user ->
                    if (user.telephoneNumber == contact.telephoneNumber) {
                        contact.hasApplication = true
                        contact.userId = user.uid
                        contact.profileUrl = user.profilePictureUrl
                    }
                }
            }
            contactsHasApplication.postValue(contactList)
        }

        return contactsHasApplication
    }

    fun getUsersById(userIdList: ArrayList<String>): LiveData<List<User>> {
        val usersLiveData = MutableLiveData<List<User>>()
        repository.getUsersById(userIdList, usersLiveData)
        return usersLiveData
    }

    fun updateToken(uid: String, token: String) {
        repository.updateToken(uid, token)
    }

    fun leaveGroup(uid: String, groupId: String) {
        repository.leaveGroup(uid, groupId)
    }
}