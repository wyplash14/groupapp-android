package com.android.ldim1933.groupup.data.model

import java.util.*

data class PollPoint(
    val pid: String? = null,
    var description: String? = null,
    var voters: ArrayList<String> = arrayListOf(),
)
