package com.android.ldim1933.groupup.common.notification

import android.util.Log
import com.android.ldim1933.groupup.data.model.PushNotification
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SendNotification {
    companion object {
        fun sendNotification(notification: PushNotification) = CoroutineScope(Dispatchers.IO).launch{
            try {
                val response = RetrofitInstance.api.postNotification(notification)
                if(response.isSuccessful) {
                    Log.d("Notification", "Response: ${Gson().toJson(response)}")
                } else {
                    Log.e("Notification", response.errorBody().toString())
                }
            } catch(e: Exception) {
                Log.e("Notification", e.toString())
            }
        }
    }
}