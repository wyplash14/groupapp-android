package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Invitation
import com.android.ldim1933.groupup.viewmodel.GroupInvitationViewModel
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.bumptech.glide.Glide


class GroupInvitationAdapter(
    private var list: MutableList<Invitation>,
    private var groupRepositoryViewModel: GroupRepositoryViewModel,
    private var groupInvitationViewModel: GroupInvitationViewModel
) : RecyclerView.Adapter<GroupInvitationAdapter.GroupViewHolder>() {

    inner class GroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var invitationTxt: TextView = itemView.findViewById(R.id.group_invitation_txt)
        var date: TextView = itemView.findViewById(R.id.group_invitation_date)
        var image: ImageView = itemView.findViewById(R.id.group_invitation_user_image)
        var acceptBtn: Button = itemView.findViewById(R.id.acceptInvBtn)
        var declineBtn: Button = itemView.findViewById(R.id.declineInvBtn)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        return GroupViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.group_invite_item, parent, false)
        )
    }

    @RequiresApi(Build.VERSION_CODES.S)
    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        val invitation: Invitation = list[position]
        val sourceString =
            "<b>" + invitation.invitedByName + "</b> "+ holder.itemView.context.getString(R.string.invitedyou)+ " <b>" + invitation.groupName + "</b> "+ holder.itemView.context.getString(R.string.invitedyoug)
        holder.invitationTxt.text = Html.fromHtml(sourceString)

        holder.date.text = invitation.createdOn

        if (invitation.invitedByPictureUrl != null) {
            Glide.with(holder.itemView.context).load(invitation.invitedByPictureUrl)
                .into(holder.image)
        }
        holder.acceptBtn.setOnClickListener {
            groupRepositoryViewModel.addUserToGroup(
                invitation.invitedTo!!,
                invitation.invitedUser!!
            )
            groupInvitationViewModel.deleteInvitation(invitation.invitationId!!)
            Toast.makeText(
                holder.acceptBtn.context,
                "Accepted invitation in ${invitation.groupName} group.",
                Toast.LENGTH_LONG
            ).show()
            list.remove(list[position])
            notifyDataSetChanged()
        }

        holder.declineBtn.setOnClickListener {
            groupInvitationViewModel.deleteInvitation(invitation.invitationId!!)
            list.remove(list[position])
            notifyDataSetChanged()
            Toast.makeText(
                holder.acceptBtn.context,
                "Declined invitation in ${invitation.groupName} group.",
                Toast.LENGTH_LONG
            ).show()
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

}