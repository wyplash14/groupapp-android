package com.android.ldim1933.groupup.view.fragment.option

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.IdGenerator
import com.android.ldim1933.groupup.common.PollType
import com.android.ldim1933.groupup.data.model.Poll
import com.android.ldim1933.groupup.databinding.AddPollPopupBinding
import com.android.ldim1933.groupup.view.adapter.GroupSpinnerAdapter
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.PollRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class BottomSheetDialogPoll : BottomSheetDialogFragment() {

    lateinit var binding: AddPollPopupBinding
    lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var groupDataViewModel: GroupRepositoryViewModel
    private lateinit var pollViewModel: PollRepositoryViewModel

    private var groupId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        groupDataViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]
        pollViewModel = ViewModelProvider(this)[PollRepositoryViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AddPollPopupBinding.inflate(inflater, container, false)

        userDataViewModel.getUserGroupsById(Firebase.auth.currentUser!!.uid)
            .observe(this.viewLifecycleOwner, {
                groupDataViewModel.getGroupsDataByIds(it)
                    .observe(this.viewLifecycleOwner, { groups ->
                        binding.spinnerPollGroup.adapter = GroupSpinnerAdapter(groups)
                        binding.spinnerPollGroup.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {}
                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    groupId = groups[position].id!!
                                }
                            }
                    })
            })

        binding.pollTitleTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isNotEmpty()) {
                        binding.pollTitleTxt.setBackgroundResource(R.drawable.add_todo_bg_light)
                    } else {
                        binding.pollTitleTxt.setBackgroundResource(R.drawable.add_todo_bg)
                    }
                    checkAllFilled()
                }
            }

        })

        binding.pollDescriptionTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isNotEmpty()) {
                        binding.pollDescriptionTxt.setBackgroundResource(R.drawable.add_todo_bg_light)
                    } else {
                        binding.pollDescriptionTxt.setBackgroundResource(R.drawable.add_todo_bg)
                    }
                    checkAllFilled()
                }
            }

        })

        binding.pollCreateButton.setOnClickListener {
            addPoll()
        }

        return binding.root
    }

    private fun checkAllFilled() {
        if (binding.pollTitleTxt.text.trim().toString().isNotEmpty() && binding.pollDescriptionTxt.text.trim()
                .toString().isNotEmpty() && groupId != null
        ) {
            binding.pollCreateButton.visibility = View.VISIBLE
        } else {
            binding.pollCreateButton.visibility = View.GONE
        }
    }

    private fun addPoll() {
        var type = PollType.NONMODIFY.type
        if (binding.checkBox.isChecked) type = PollType.MODIFY.type
        if (binding.pollTitleTxt.text.trim().length > 255) {
            binding.pollTitleTxt.error =
                "Text size should be max 255 characters. Current size is: ${binding.pollTitleTxt.text.trim().length}."
            binding.pollTitleTxt.requestFocus()
        } else {
            pollViewModel.create(
                Poll(
                    id = IdGenerator.generate(Firebase.auth.currentUser!!.uid, "poll", ""),
                    creatorId = Firebase.auth.currentUser!!.uid,
                    groupId = groupId!!,
                    title = binding.pollTitleTxt.text.trim().toString(),
                    description = binding.pollDescriptionTxt.text.trim().toString(),
                    type = type
                )
            )
        }
    }

}