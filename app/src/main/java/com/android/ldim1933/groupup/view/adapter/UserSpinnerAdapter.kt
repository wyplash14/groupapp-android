package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.User
import com.bumptech.glide.Glide

class UserSpinnerAdapter(private val list: ArrayList<User>) : BaseAdapter() {
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("SetTextI18n", "ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rootView =
            LayoutInflater.from(parent!!.context).inflate(R.layout.member_item, parent, false)
        val name: TextView = rootView.findViewById(R.id.person_item_name)
        val image: ImageView = rootView.findViewById(R.id.person_item_image)

        name.text = "${list[position].firstName}  ${list[position].lastName}"

        if (list[position].profilePictureUrl != null) {
            Glide.with(image.context).load(list[position].profilePictureUrl)
                .into(image)
        }
        return rootView
    }
}