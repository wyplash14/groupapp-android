package com.android.ldim1933.groupup.view.fragment.option

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ContentResolver
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Contact
import com.android.ldim1933.groupup.databinding.PopupInviteMembersBinding
import com.android.ldim1933.groupup.view.adapter.ContactAdapter
import com.android.ldim1933.groupup.viewmodel.ContactsViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.util.*
import kotlin.collections.ArrayList


class BottomSheetDialogCommonContact : BottomSheetDialogFragment() {

    private lateinit var adapter: ContactAdapter
    private lateinit var binding: PopupInviteMembersBinding
    private var contactList = ArrayList<Contact>()
    private lateinit var contactSet: MutableSet<Contact>

    private lateinit var dialogInvite: Dialog
    private lateinit var contactViewModel: ContactsViewModel

    private var contact: Contact? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contactViewModel = ViewModelProvider(this)[ContactsViewModel::class.java]

        dialogInvite = this.context?.let { Dialog(it) }!!
        dialogInvite.setContentView(R.layout.invitation_dialog)
        dialogInvite.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialogInvite.window?.attributes?.windowAnimations = R.style.animationDialog
        dialogInvite.setCancelable(false)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PopupInviteMembersBinding.inflate(inflater, container, false)
        binding.switchContact.visibility = View.GONE
        contactSet = mutableSetOf()
        getContacts()
        contactList.addAll(contactSet)

        adapter = ContactAdapter(contactList)
        binding.contactRecycleView.adapter = adapter
        binding.inviteMembersTitle.text = "Add new contact"


        adapter.setOnItemClickListener(object : ContactAdapter.OnItemClickListener {
            @RequiresApi(Build.VERSION_CODES.O)
            @SuppressLint("SetTextI18n")
            override fun onClick(position: Int) {
                contact = adapter.getContact(position)

                val textView: TextView = dialogInvite.findViewById(R.id.textViewInvite)
                textView.text = "Add contact ${adapter.getContact(position).name} to ${
                    arguments?.getString("groupName")
                }."

                dialogInvite.show()
            }
        })

        binding.searchContactTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                filter(s.toString())
                Log.i("FILTER", "text has changed")
            }

        })


        val inviteBtn = dialogInvite.findViewById<Button>(R.id.btn_invite)
        inviteBtn.text = "Yes"
        inviteBtn.setOnClickListener {
            val groupId = arguments?.getString("groupId")
            if (groupId != null) {
                contact?.let { it1 -> contactViewModel.create(it1, groupId) }
            }
            dialogInvite.dismiss()
        }

        dialogInvite.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            dialogInvite.dismiss()
        }

        return binding.root
    }

    private fun filter(text: String) {
        val filteredList = ArrayList<Contact>()
        adapter.getList().forEach { contact ->
            if (contact.name?.lowercase(Locale.getDefault())?.contains(text) == true) {
                filteredList.add(contact)
            }
        }
        adapter.filterList(filteredList)
    }

    @SuppressLint("Range")
    private fun getContacts() {
        if (ContextCompat.checkSelfPermission(
                binding.root.context,
                android.Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            this.activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf(android.Manifest.permission.READ_CONTACTS), 0
                )
            }
        }

        val contentResolver: ContentResolver = this.activity!!.contentResolver
        val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val cursor = contentResolver.query(uri, null, null, null, null)

        if (cursor != null) {
            if (cursor.count > 0) {
                while (cursor.moveToNext()) {
                    val contactName =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                    val contactNumber =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    val contactPhoto =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                    val newPhoneNumber =
                        contactNumber.replace("(", "").replace(")", "").replace("+", "")
                            .replace(" ", "").trim()
                    contactSet.add(
                        Contact(
                            name = contactName,
                            image = contactPhoto,
                            telephoneNumber = newPhoneNumber
                        )
                    )
                }
            }
        }
        cursor?.close()
    }
}