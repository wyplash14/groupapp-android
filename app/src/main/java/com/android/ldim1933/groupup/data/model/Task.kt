package com.android.ldim1933.groupup.data.model

data class Task(
    val id: String? = null,
    val groupId: String? = null,
    val assignedToId: String? = null,
    val assignedById: String? = null,
    val description: String? = null,
    val dueTime: String? = null,
    var state: String? = null,

    var groupName: String? = null,
    var userName: String? = null,
    var userImage: String? = null,
    var toUserName: String? = null,
    var toUserImage: String? = null,
    var groupImage: String? = null,
)
