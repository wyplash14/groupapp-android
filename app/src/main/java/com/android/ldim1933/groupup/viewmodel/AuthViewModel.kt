package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.data.repository.AuthRepository
import com.google.firebase.auth.FirebaseUser

class AuthViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: AuthRepository = AuthRepository(application)
    var userData = repository.firebaseUserMutableLiveData

    fun register(email: String, password: String): MutableLiveData<FirebaseUser> {
        repository.register(email, password)
        return userData
    }

    fun logIn(email: String, password: String) {
        repository.logIn(email, password)
    }

    fun signOut() {
        repository.signOut()
    }

}