package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.data.model.Contact
import com.android.ldim1933.groupup.data.repository.ContactRepository

class ContactsViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = ContactRepository(application)
    private val _contactLiveData = MutableLiveData<List<Contact>>()
    private var contactLiveData: LiveData<List<Contact>> = _contactLiveData

    fun create(contact: Contact, groupId: String) {
        repository.create(contact, groupId)
    }

    fun getContactsOfGroup(groupId: String): LiveData<List<Contact>> {
        repository.getContactsOfGroup(groupId, _contactLiveData)
        return contactLiveData
    }
}