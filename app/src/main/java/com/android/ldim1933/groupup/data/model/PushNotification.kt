package com.android.ldim1933.groupup.data.model

data class PushNotification(
    val data: NotificationData,
    val to: String
)
