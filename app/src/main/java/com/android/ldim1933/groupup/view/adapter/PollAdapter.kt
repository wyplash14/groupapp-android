package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.data.model.Poll

class PollAdapter(private val list: MutableList<Poll>) :
    RecyclerView.Adapter<PollAdapter.PollViewHolder>() {
    private lateinit var mListener: OnItemClickListener

    private var originalNotFilteredList: MutableList<Poll> = mutableListOf()

    init {
        originalNotFilteredList.addAll(list)
    }

    interface OnItemClickListener {
        fun onClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }


    class PollViewHolder(itemView: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.poll_item_name)
        val createdBy: TextView = itemView.findViewById(R.id.poll_item_creator_name)
        val groupName: TextView = itemView.findViewById(R.id.poll_item_group_name)
        val background: ConstraintLayout = itemView.findViewById(R.id.poll_background)
        val votesNum: TextView = itemView.findViewById(R.id.poll_item_votes)
        val progressBar: ProgressBar = itemView.findViewById(R.id.poll_progress_bar)

        init {
            itemView.setOnClickListener {
                listener.onClick(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PollViewHolder {
        return PollViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.poll_item, parent, false),
            mListener
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PollViewHolder, position: Int) {
        holder.title.text = list[position].title
        holder.createdBy.text = holder.itemView.context.getString(R.string.created_by) + " " + list[position].creatorUserName
        holder.groupName.text = holder.itemView.context.getString(R.string.group) + " " + list[position].groupName

        val votedList = ArrayList<String>()
        list[position].pollPoint.forEach { pollPoint ->
            pollPoint.voters.forEach { voter ->
                if (!votedList.contains(voter)) {
                    votedList.add(voter)
                }
            }
        }

        holder.votesNum.text = votedList.size.toString() + " " + holder.itemView.context.getString(R.string.vote)

        when (position % 3) {
            0 -> holder.background.background =
                ContextCompat.getColor(holder.itemView.context, R.color.p_yellow).toDrawable()
            1 -> holder.background.background =
                ContextCompat.getColor(holder.itemView.context, R.color.p_purple).toDrawable()
            2 -> holder.background.background =
                ContextCompat.getColor(holder.itemView.context, R.color.p_green).toDrawable()
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        list.clear()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPollsOfGroup(group: Group) {
        list.clear()
        Log.i("GROUP", group.toString())
        Log.i("List", list.toString())
        Log.i("NOTFList", originalNotFilteredList.toString())
        if (group.id.equals(Constants.ALL_GROUPS_ID)) {
            list.addAll(originalNotFilteredList)
        } else {
            list.addAll(originalNotFilteredList.filter { it.groupName == group.name && it.groupImage == group.profilePictureUrl })
        }
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addPollList(mutableList: MutableList<Poll>) {
        list.clear()
        list.addAll(mutableList)
        originalNotFilteredList.clear()
        originalNotFilteredList.addAll(list)
        notifyDataSetChanged()
    }

    fun getPolls(): MutableList<Poll> {
        return list
    }

}