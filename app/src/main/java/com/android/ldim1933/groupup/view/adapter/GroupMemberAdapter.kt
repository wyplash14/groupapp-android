package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.User
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class GroupMemberAdapter(
    private var list: MutableList<User>,
) :
    RecyclerView.Adapter<GroupMemberAdapter.PersonViewHolder>() {

    inner class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.person_item_name)
        var image: CircleImageView = itemView.findViewById(R.id.person_item_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        return PersonViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.member_item, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val user: User = list[position]
        holder.name.text = "${user.firstName} ${user.lastName}"
        if (user.profilePictureUrl != null) {
            Glide.with(holder.itemView.context).load(user.profilePictureUrl)
                .into(holder.image)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addList(usersList: MutableList<User>) {
        list.clear()
        list.addAll(usersList)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): User {
        return list[position]
    }

    fun remove(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }
}