package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.data.model.Invitation
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class GroupInvitationRepository(private val application: Application) {
    private val database =
        Firebase.database("https://groupup-6ed7f-default-rtdb.europe-west1.firebasedatabase.app/")
    private val iData = database.getReference(Constants.INVITATIONS)
    private val gData = database.getReference(Constants.GROUPS)
    private val uData = database.getReference(Constants.USERS)
    private val invitationArray = ArrayList<Invitation>()

    fun create(invitation: Invitation) {
        invitation.invitationId?.let { it ->
            iData.child(it).setValue(invitation).addOnSuccessListener {
                Toast.makeText(
                    application,
                    application.getString(R.string.invitation),
                    Toast.LENGTH_LONG
                ).show()
            }.addOnFailureListener {
                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }
        }
    }

    fun getUsersInvitations(invitedUserId: String, invitationData:MutableLiveData<ArrayList<Invitation>>) {
        iData.orderByChild("invitedUser").equalTo(invitedUserId)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(it: DataSnapshot) {
                    it.children.forEach { child ->
                        val invitation: HashMap<String, String> =
                            child.value as HashMap<String, String>
                        val invitationId = invitation["invitationId"]
                        val invitedBy = invitation["invitedBy"]
                        val invitedTo = invitation["invitedTo"]
                        val createdOn = invitation["createdOn"]

                        val tempInvitation = Invitation(
                            invitationId = invitationId!!,
                            invitedUser = invitedUserId,
                            invitedBy = invitedBy!!,
                            invitedTo = invitedTo!!,
                            createdOn = createdOn
                        )

                        gData.child(invitedTo).get().addOnCompleteListener {
                            if (it.isSuccessful) {
                                if (it.result!!.exists()) {
                                    tempInvitation.groupName =
                                        it.result!!.getValue(Group::class.java)!!.name

                                    uData.child(invitedBy).get().addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            if (it.result!!.exists()) {
                                                val firstname =
                                                    it.result!!.child("firstName").value.toString()
                                                val lastname =
                                                    it.result!!.child("lastName").value.toString()
                                                val profilePictureUrl =
                                                    it.result!!.child("profilePictureUrl").value.toString()
                                                tempInvitation.invitedByPictureUrl =
                                                    profilePictureUrl
                                                tempInvitation.invitedByName =
                                                    "$firstname $lastname"
                                                invitationArray.add(tempInvitation)
                                                invitationData.postValue(invitationArray)
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
                }
            })
    }

    fun deleteInvitation(invitationId: String) {
        iData.child(invitationId).removeValue().addOnCompleteListener {
            Log.i(
                "Removed invitation",
                "The invitation was removed"
            )
        }
    }

}