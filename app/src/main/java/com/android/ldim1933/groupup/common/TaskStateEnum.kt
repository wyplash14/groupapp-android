package com.android.ldim1933.groupup.common

enum class TaskStateEnum(val title: String) {
    PENDING("pending"),
    ACCEPTED("accepted"),
    DECLINED("declined"),
    DONE("done"),
}