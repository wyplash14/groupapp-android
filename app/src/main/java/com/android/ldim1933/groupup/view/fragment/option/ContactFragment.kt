package com.android.ldim1933.groupup.view.fragment.option

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.provider.Telephony
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.databinding.FragmentContactBinding
import com.android.ldim1933.groupup.view.adapter.CommonContactAdapter
import com.android.ldim1933.groupup.view.adapter.GroupSpinnerAdapter2
import com.android.ldim1933.groupup.viewmodel.ContactsViewModel
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

class ContactFragment : Fragment() {

    private lateinit var binding: FragmentContactBinding
    private lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var groupDataViewModel: GroupRepositoryViewModel
    private lateinit var contactsViewModel: ContactsViewModel

    private var contactAdapter = CommonContactAdapter(arrayListOf())

    private var groupId: String? = null
    private var groupName: String? = null
    private var telephoneNumber: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        groupDataViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]
        contactsViewModel = ViewModelProvider(this)[ContactsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentContactBinding.inflate(inflater, container, false)

        binding.refreshLayout.setOnRefreshListener {
            val fragment = ContactFragment()
            fragmentManager!!.beginTransaction().replace(this.id, fragment).commit()
            binding.refreshLayout.isRefreshing = false
        }

        binding.addContactButton.setOnClickListener {
            val bs = BottomSheetDialogCommonContact()
            val bundle = Bundle()
            bundle.putString("groupId", groupId)
            bundle.putString("groupName", groupName)
            bs.arguments = bundle
            bs.show(childFragmentManager, "Add")
        }
        loadSpinner()

        return binding.root
    }

    fun loadSpinner() {
        userDataViewModel.getUserGroupsById(Firebase.auth.currentUser!!.uid)
            .observe(this.viewLifecycleOwner, {
                groupDataViewModel.getGroupsDataByIds(it)
                    .observe(this.viewLifecycleOwner, { groups ->
                        binding.spinnerContactGroup.adapter = GroupSpinnerAdapter2(groups)
                        binding.spinnerContactGroup.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    groupId = groups[position].id!!
                                    groupName = groups[position].name!!
                                    loadCommonContacts(groupId!!)
                                }
                            }
                    })
            })
    }

    private fun loadCommonContacts(groupId: String) {
        contactAdapter = CommonContactAdapter(arrayListOf())
        binding.contactRecycleView.adapter = contactAdapter

        binding.noContactTxt.visibility = View.VISIBLE
        contactsViewModel.getContactsOfGroup(groupId).observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                binding.noContactTxt.visibility = View.GONE
            } else {
                binding.noContactTxt.visibility = View.VISIBLE
            }
            Log.i("CONTACTS", it.toString())
            contactAdapter.setContactList(it)
            binding.contactRecycleView.adapter = contactAdapter
        })


        val itemTouchHelper = ItemTouchHelper(simpleCallback)
        itemTouchHelper.attachToRecyclerView(binding.contactRecycleView)
    }


    private val simpleCallback = object :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.adapterPosition
            telephoneNumber = contactAdapter.getContact(position).telephoneNumber!!
            when (direction) {
                ItemTouchHelper.LEFT -> makeACall()
                else -> openSMS()
            }
            loadSpinner()
        }

        override fun onChildDraw(
            c: Canvas,
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            dX: Float,
            dY: Float,
            actionState: Int,
            isCurrentlyActive: Boolean
        ) {
            RecyclerViewSwipeDecorator.Builder(
                c,
                recyclerView,
                viewHolder,
                dX,
                dY,
                actionState,
                isCurrentlyActive
            )
                .addSwipeRightBackgroundColor(
                    ContextCompat.getColor(viewHolder.itemView.context, R.color.yellow)
                )
                .addSwipeLeftActionIcon(R.drawable.ic_baseline_call_24)
                .addSwipeLeftBackgroundColor(
                    ContextCompat.getColor(viewHolder.itemView.context, R.color.green_light)
                )
                .addSwipeRightActionIcon(R.drawable.sms_icon)
                .create()
                .decorate()

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        }
    }

    private fun openSMS() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", telephoneNumber, null))
        intent.setPackage(Telephony.Sms.getDefaultSmsPackage(context))
        startActivity(intent)
    }

    private fun makeACall() {
        if (telephoneNumber != null) {
            val dial = "tel:$telephoneNumber"
            if (ContextCompat.checkSelfPermission(
                    this.context!!,
                    android.Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this.activity!!,
                    arrayOf(android.Manifest.permission.CALL_PHONE), 10
                )
            } else {
                startActivity(Intent(Intent.ACTION_CALL, Uri.parse(dial)))
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 10) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeACall()
            } else {
                Toast.makeText(this.context, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

}