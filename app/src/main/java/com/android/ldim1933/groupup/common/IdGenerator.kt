package com.android.ldim1933.groupup.common

import java.text.SimpleDateFormat
import java.util.*

class IdGenerator {
    companion object {
        fun generate(id: String, type: String, name: String): String {
            val formatter = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
            val now = Date()
            return id + type + formatter.format(now) + name
        }
    }
}