package com.android.ldim1933.groupup.data.model

data class User(
    var uid: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var telephoneNumber: String? = null,
    var email: String? = null,
    var profilePictureUrl: String? = null,
    var groups: ArrayList<String> = arrayListOf(),
    var token: String? = null,
    var validated: String? = null
)