package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.NotificationData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class NotificationRepository(private val application: Application) {
    private val database =
        Firebase.database("https://groupup-6ed7f-default-rtdb.europe-west1.firebasedatabase.app/")
    private val nData = database.getReference(Constants.NOTIFICATIONS)
    private val uData = database.getReference(Constants.USERS)
    private val gData = database.getReference(Constants.GROUPS)

    fun create(notification: NotificationData) {
        nData.child(notification.assignedToId!!).child(notification.id!!).setValue(notification)
            .addOnSuccessListener {
                Toast.makeText(
                    application,
                    application.getString(R.string.notificationd),
                    Toast.LENGTH_LONG
                ).show()
            }.addOnFailureListener {
                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
        }
    }

    fun getUserNotifications(uid: String, liveData: MutableLiveData<List<NotificationData>>) {
        val list = ArrayList<NotificationData>()
        nData.child(uid).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach { child ->
                    val notificationData = child.getValue(NotificationData::class.java)
                    gData.child(notificationData!!.groupId!!).get().addOnSuccessListener { group ->
                        notificationData.groupName = group.child("name").value.toString()
                        notificationData.groupImage =
                            group.child("profilePictureUrl").value.toString()
                        uData.child(notificationData.assignedById!!).get()
                            .addOnSuccessListener { user ->
                                notificationData.userName =
                                    user.child("firstName").value.toString() + " " + user.child("lastName").value.toString()
                                notificationData.userImage =
                                    user.child("profilePictureUrl").value.toString()
                                uData.child(notificationData.assignedToId!!).get()
                                    .addOnSuccessListener { toUser ->
                                        notificationData.toUserName =
                                            toUser.child("firstName").value.toString() + " " + user.child(
                                                "lastName"
                                            ).value.toString()
                                        notificationData.toUserImage =
                                            toUser.child("profilePictureUrl").value.toString()
                                        list.add(notificationData)
                                        liveData.postValue(list)
                                    }
                            }
                    }
                }
                list.clear()
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }
        })
    }

    fun getGivenNotifications(uid: String, liveData: MutableLiveData<List<NotificationData>>) {
        val list = ArrayList<NotificationData>()
        nData.ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach { child ->
                    nData.child(child.key.toString()).orderByChild("assignedById").equalTo(uid)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(it: DataSnapshot) {
                                it.children.forEach { element ->

                                    val notificationData =
                                        element.getValue(NotificationData::class.java)

                                    gData.child(notificationData!!.groupId!!).child("name").get()
                                        .addOnSuccessListener { groupName ->
                                            notificationData.groupName = groupName.value as String
                                            gData.child(notificationData.groupId!!)
                                                .child("profilePictureUrl").get()
                                                .addOnSuccessListener { groupImage ->
                                                    notificationData.groupImage =
                                                        groupImage.value as String
                                                    uData.child(notificationData.assignedById!!)
                                                        .child("profilePictureUrl").get()
                                                        .addOnSuccessListener { user ->
                                                            notificationData.userImage =
                                                                user.value as String
                                                            uData.child(notificationData.assignedToId!!)
                                                                .get()
                                                                .addOnSuccessListener { toUser ->
                                                                    notificationData.toUserName =
                                                                        toUser.child("firstName").value.toString() + " " + toUser.child(
                                                                            "lastName"
                                                                        ).value.toString()
                                                                    notificationData.toUserImage =
                                                                        toUser.child("profilePictureUrl").value.toString()
                                                                    list.add(notificationData)
                                                                    liveData.postValue(list)
                                                                }
                                                        }
                                                }
                                        }
                                }

                            }

                            override fun onCancelled(error: DatabaseError) {
                                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
                            }
                        })
                }
                list.clear()
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }

        })
    }

}
