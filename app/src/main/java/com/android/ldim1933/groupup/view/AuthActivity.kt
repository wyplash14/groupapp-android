package com.android.ldim1933.groupup.view

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.User
import com.android.ldim1933.groupup.databinding.ActivityAuthBinding
import com.android.ldim1933.groupup.viewmodel.AuthViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.hbb20.CountryCodePicker
import de.hdodenhof.circleimageview.CircleImageView
import java.util.concurrent.TimeUnit

class AuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthBinding
    private lateinit var email: TextView
    private lateinit var password: TextView
    private lateinit var passwordAgain: TextView
    private lateinit var viewModel: ViewModel
    private lateinit var userDataViewModel: UserRepositoryViewModel

    private lateinit var registerPopupView: View
    private var imageUri: Uri? = null
    private var sentCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]

        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[AuthViewModel::class.java]

        binding.loginBtn.setOnClickListener {
            email = binding.emailTxt
            password = binding.passwordTxt
            if (validateValuesForLogin(email, password)) {
                (viewModel as AuthViewModel).logIn(
                    email.text.trim().toString(),
                    password.text.toString().trim()
                )
            }
        }

        binding.forgotPwBtn.setOnClickListener {
            createRestorationDialog()
        }

        binding.signupBtn.setOnClickListener {
            createRegisterContactDialog()
        }
    }

    private fun createRestorationDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val restorePopupView = layoutInflater.inflate(R.layout.reset_pw_dialog, null)
        email = restorePopupView.findViewById(R.id.emailRegisterResetTxt)

        restorePopupView.findViewById<Button>(R.id.btn_restoration).setOnClickListener {
            if (email.text.isEmpty()) {
                email.error = "Fill with your email."
                email.requestFocus()
            } else {
                Firebase.auth.sendPasswordResetEmail(email.text.toString()).addOnSuccessListener {
                    Toast.makeText(this, "Reset Link Sent To Your Email.", Toast.LENGTH_LONG).show()
                }
            }
        }

        dialogBuilder.setView(restorePopupView)
        val dialog = dialogBuilder.create()
        dialog.show()
    }

    private fun createRegisterContactDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val registerPopupView = layoutInflater.inflate(R.layout.register_popup, null)

        email = registerPopupView.findViewById(R.id.emailRegisterTxt)
        password = registerPopupView.findViewById(R.id.passwordRegisterTxt)
        passwordAgain = registerPopupView.findViewById(R.id.passwordAgainRegisterTxt)


        viewModel = ViewModelProvider(this)[AuthViewModel::class.java]

        dialogBuilder.setView(registerPopupView)
        val dialog = dialogBuilder.create()
        dialog.show()

        registerPopupView.findViewById<Button>(R.id.registerBtn).setOnClickListener {
            if (validateValuesForRegister(email, password, passwordAgain)) {
                createUserContactDialog()
                dialog.dismiss()
            }
        }

    }

    private fun validateValuesForLogin(email: TextView, password: TextView): Boolean {
        if (!isEmptyValidation(email) || !isEmptyValidation(password)) return false
        if (!lengthValidation(email) || !lengthValidation(password)) return false

        return true
    }

    private fun validateValuesForRegister(
        email: TextView,
        password: TextView,
        passwordAgain: TextView
    ): Boolean {
        if (!isEmptyValidation(email) || !isEmptyValidation(password)
            || !isEmptyValidation(passwordAgain)
        ) return false


        if (!lengthValidation(email) || !lengthValidation(password) || !
            lengthValidation(passwordAgain)
        ) return false

        if (!passwordValidation(password, passwordAgain)) return false

        if (!emailPatternValidation(email)) return false

        return true
    }

    private fun emailPatternValidation(email: TextView): Boolean {
        val matcher = Regex(Patterns.EMAIL_ADDRESS.pattern())

        if (matcher.find(email.text.trim()) == null) {
            email.requestFocus()
            return false
        }

        return true
    }

    private fun passwordValidation(password: TextView, passwordAgain: TextView): Boolean {
        val matcher = Regex(Constants.PASSWORD_PATTERN)

        if (matcher.find(password.text.trim()) == null) {
            password.error =
                "Password should contain an uppercase a lowercase character a number and a special character. I should be at least 6 length long."
            password.requestFocus()
            return false
        }

        if (password.text.trim().toString() != passwordAgain.text.trim().toString()) {
            Log.i("password", "P" + password.text.toString() + " " + passwordAgain.text.toString())
            passwordAgain.error = "The two password not matching."
            passwordAgain.requestFocus()
            return false
        }

        return true
    }

    private fun lengthValidation(field: TextView): Boolean {
        if (field.length() > 255) {
            field.error = "This field's length should be less than 255 characters"
            field.requestFocus()
            return false
        }
        return true
    }

    private fun isEmptyValidation(field: TextView): Boolean {
        if (field.text.isEmpty()) {
            field.error = "This field should be completed"
            field.requestFocus()
            return false
        }
        return true
    }

    @SuppressLint("InflateParams")
    private fun createUserContactDialog() {

        val dialogBuilder = AlertDialog.Builder(this)
        registerPopupView = layoutInflater.inflate(R.layout.add_user_popup, null)

        val firstname = registerPopupView.findViewById<EditText>(R.id.firstnameTxt)
        val lastname = registerPopupView.findViewById<EditText>(R.id.lastnameTxt)
        val telNum = registerPopupView.findViewById<EditText>(R.id.telephoneTxt)
        val telValidation = registerPopupView.findViewById<EditText>(R.id.validationEditText)
        val validateBtn = registerPopupView.findViewById<Button>(R.id.validationBtn)
        val uploadPicture =
            registerPopupView.findViewById<CircleImageView>(R.id.uploadProfilePicture)
        val countryCode = registerPopupView.findViewById<CountryCodePicker>(R.id.countryCodePicker)

        telNum.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) {
                    if (s.isNotEmpty()) {
                        telValidation.visibility = View.VISIBLE
                        validateBtn.visibility = View.VISIBLE
                    } else {
                        telValidation.visibility = View.GONE
                        validateBtn.visibility = View.GONE
                    }
                }
            }
        })

        dialogBuilder.setView(registerPopupView)
        val dialog = dialogBuilder.create()
        dialog.setCancelable(false)
        dialog.show()

        registerPopupView.findViewById<ImageView>(R.id.uploadProfilePicture).setOnClickListener {
            selectImage()
        }

        registerPopupView.findViewById<ImageView>(R.id.uploadProfilePictureSchema)
            .setOnClickListener {
                selectImage()
            }

        registerPopupView.findViewById<Button>(R.id.validationBtn).setOnClickListener {
            countryCode.registerCarrierNumberEditText(telNum)
            sendVerificationCodeToUser(countryCode.fullNumberWithPlus)

        }

        registerPopupView.findViewById<Button>(R.id.finishBtn).setOnClickListener {
            if (isEmptyValidation(firstname) && isEmptyValidation(lastname) && isEmptyValidation(
                    telNum
                )
            ) {
                if (imageUri == null) {
                    uploadPicture.requestFocus()
                    Toast.makeText(this, "Please select a profile picture.", Toast.LENGTH_LONG)
                        .show()
                } else {
                    if (sentCode.isNotEmpty() && telValidation.text.toString().isNotEmpty()) {
                        val cred = PhoneAuthProvider.getCredential(
                            sentCode,
                            telValidation.text.toString()
                        )
                        Firebase.auth.signInWithCredential(cred).addOnCompleteListener {
                            if (it.isSuccessful) {
                                Firebase.auth.currentUser!!.delete()
                                (viewModel as AuthViewModel).register(
                                    email.text.trim().toString(),
                                    password.text.toString().trim()
                                ).observe(this, {
                                    userDataViewModel.create(
                                        User(
                                            uid = Firebase.auth.currentUser?.uid,
                                            firstName = firstname.text.toString(),
                                            lastName = lastname.text.toString(),
                                            telephoneNumber = countryCode.fullNumberWithPlus,
                                            validated = "true",
                                        ), imageUri
                                    )
                                    Toast.makeText(this, "Registration ended", Toast.LENGTH_LONG)
                                        .show()
                                    startActivity(Intent(this, MainActivity::class.java))
                                    dialog.dismiss()
                                    this.finish()
                                })
                            } else {
                                telValidation.error = "Verification code is not correct."
                                telValidation.requestFocus()
                            }
                        }
                    }
                }
            }
        }

        registerPopupView.findViewById<Button>(R.id.cancel_registration_btn).setOnClickListener {
            dialog.dismiss()
        }
    }

    private val mCallBacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onCodeSent(code: String, p1: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(code, p1)
            sentCode = code
        }

        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
            Log.i("AuthPhone", phoneAuthCredential.toString())

        }

        override fun onVerificationFailed(e: FirebaseException) {
            Log.e("ERROR", e.message.toString())
            Toast.makeText(this@AuthActivity, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendVerificationCodeToUser(phoneNumber: String) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phoneNumber,
            60,
            TimeUnit.SECONDS,
            this,
            mCallBacks
        )
    }

    private fun selectImage() {
        val intent = Intent()

        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == RESULT_OK) {
            registerPopupView.findViewById<ImageView>(R.id.uploadProfilePicture)
                .setImageURI(data!!.data)
            registerPopupView.findViewById<ImageView>(R.id.uploadProfilePictureSchema).visibility =
                View.VISIBLE
            imageUri = data.data!!
        }
    }
}

