package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.Contact
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class ContactRepository(private val application: Application) {

    private val database =
        Firebase.database("https://groupup-6ed7f-default-rtdb.europe-west1.firebasedatabase.app/")
    private val mData = database.getReference(Constants.CONTACTS)

    fun create(contact: Contact, groupId: String) {
        mData.child(groupId).push().setValue(contact).addOnSuccessListener {
            Toast.makeText(
                application,
                application.getString(R.string.contact),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun getContactsOfGroup(groupId: String, mutableLiveData: MutableLiveData<List<Contact>>) {
        val list = mutableListOf<Contact>()
        mData.child(groupId).get().addOnSuccessListener {
            mutableLiveData.postValue(list)
            it.children.forEach { contact ->
                val contactTemp = contact.getValue(Contact::class.java)!!
                list.add(contactTemp)
                mutableLiveData.postValue(list)
            }
        }
    }
}