package com.android.ldim1933.groupup.view.fragment.menu

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.databinding.FragmentSettingsBinding
import com.android.ldim1933.groupup.view.fragment.dashboard.DashboardFragment
import java.util.*

class SettingsFragment : Fragment() {
    private lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentSettingsBinding.inflate(inflater, container, false)

        themeSettings()
        languageSettings()
        return binding.root
    }

    private fun languageSettings() {
        if(resources.configuration.locale.language.equals("en")){
            binding.radioButtonEnglish.isChecked = true
        }
        else{
            binding.radioButtonHungarian.isChecked = true
        }

        binding.radioGroupLanguage.setOnCheckedChangeListener { group, checkedId ->
            val sharedLanguage = this.activity!!.getSharedPreferences("language",0)
            when (checkedId) {
                R.id.radioButtonHungarian -> {
                    val editor = sharedLanguage.edit()
                    editor.putString("language", "hu")
                    editor.apply()
                    setLanguage("hu")
                }
                else -> {
                    val editor = sharedLanguage.edit()
                    editor.putString("language", "en")
                    editor.apply()
                    setLanguage("hu")
                    setLanguage("en")
                }
            }
        }
    }

    private fun setLanguage(language: String) {
        val resources = resources
        val metrics = resources.displayMetrics
        val configuration = resources.configuration
        configuration.locale = Locale(language)
        resources.updateConfiguration(configuration,metrics)
        onConfigurationChanged(configuration)
        binding.textViewSettingsTittle.setText(R.string.groupup_settings)
        binding.themeTxt.setText(R.string.themes)
        binding.switchTheme.setText(R.string.dark_theme)
        binding.languageTxt.setText(R.string.language)
        binding.radioButtonHungarian.setText(R.string.hungarian)
        binding.radioButtonEnglish.setText(R.string.english)
    }

    private fun themeSettings() {

        val sharedPrefferences = this.activity!!.getSharedPreferences("night", 0)
        val isNightModeOn = sharedPrefferences.getBoolean("night_mode", false)
        binding.switchTheme.isChecked = isNightModeOn

        binding.switchTheme.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                binding.switchTheme.isChecked = true
                val editor = sharedPrefferences.edit()
                editor.putBoolean("night_mode", true)
                editor.apply()
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                binding.switchTheme.isChecked = false
                val editor = sharedPrefferences.edit()
                editor.putBoolean("night_mode", false)
                editor.apply()
            }
        }

    }


}