package com.android.ldim1933.groupup.common.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.NavDeepLinkBuilder
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.view.MainActivity

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        intent!!.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        val pendingIntent = NavDeepLinkBuilder(context!!)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.navigation_graph)
            .setDestination(R.id.notificationFragment)
            .createPendingIntent()

        val builder = NotificationCompat.Builder(context, "alarm")
            .setSmallIcon(R.drawable.groupup_icon)
            .setContentTitle("GroupUp reminder. Don't forget to...")
            .setContentText(intent.extras?.get("Message").toString())
            .setAutoCancel(true)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)

        val notificationCompat = NotificationManagerCompat.from(context)

        notificationCompat.notify(123, builder.build())
    }
}