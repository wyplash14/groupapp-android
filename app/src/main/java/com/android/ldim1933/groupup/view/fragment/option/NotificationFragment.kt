package com.android.ldim1933.groupup.view.fragment.option

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.common.DetailedTodoPopup
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.data.model.NotificationData
import com.android.ldim1933.groupup.data.model.Task
import com.android.ldim1933.groupup.databinding.FragmentNotificationBinding
import com.android.ldim1933.groupup.view.adapter.GroupSpinnerAdapter2
import com.android.ldim1933.groupup.view.adapter.NotificationAdapter
import com.android.ldim1933.groupup.viewmodel.NotificationViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.*

class NotificationFragment : Fragment() {
    lateinit var binding: FragmentNotificationBinding
    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var notificationMutableList: MutableList<NotificationData>
    private lateinit var adapter: NotificationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notificationViewModel = ViewModelProvider(this)[NotificationViewModel::class.java]
        notificationMutableList = emptyArray<NotificationData>().toMutableList()
        adapter = NotificationAdapter(notificationMutableList)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNotificationBinding.inflate(inflater, container, false)

        binding.addNotificationButton.setOnClickListener {
            val bs = BottomSheetDialogNotification()
            bs.show(childFragmentManager, "Add")
        }

        loadGivenNotifications()

        return binding.root
    }

    private fun loadGivenNotifications() {
        binding.notificationRecyclerView.adapter = adapter

        Firebase.auth.currentUser?.let { user ->
            notificationViewModel.getGivenNotification(user.uid)
                .observe(viewLifecycleOwner, { notList ->
                    adapter.clear()
                    adapter.setNotificationList(notList as MutableList<NotificationData>)
                    binding.notificationRecyclerView.adapter = adapter

                    adapter.setOnItemClickListener(object :
                        NotificationAdapter.OnItemClickListener {
                        override fun onClick(position: Int) {
                            this@NotificationFragment.context?.let { it1 ->
                                DetailedTodoPopup(it1).showInPopup(
                                    convertNotificationToTask(notificationMutableList[position])
                                )
                            }
                        }

                    })
                    val groupList = mutableSetOf<Group>()
                    groupList.add(Group(name = Constants.ALL_GROUPS, id = Constants.ALL_GROUPS_ID))
                    notificationMutableList.forEach {
                        groupList.add(
                            Group(
                                name = it.groupName,
                                profilePictureUrl = it.groupImage
                            )
                        )
                    }
                    binding.spinnerNotificationGroup.adapter =
                        GroupSpinnerAdapter2(groupList.toMutableList() as ArrayList<Group>)

                    binding.spinnerNotificationGroup.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {

                            }

                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                adapter.setNotificationListOfGroup(groupList.toMutableList()[position])
                            }
                        }
                })
        }
    }

    fun convertNotificationToTask(notification: NotificationData): Task {
        return Task(
            id = notification.id ?: "",
            assignedToId = notification.assignedToId ?: "",
            assignedById = notification.assignedById ?: "",
            groupName = notification.groupName,
            description = notification.message ?: "",
            groupImage = notification.groupImage,
            userName = notification.userName,
            userImage = notification.userImage,
            toUserImage = notification.toUserImage,
            toUserName = notification.toUserName,
            dueTime = notification.time ?: "",
            groupId = notification.groupId ?: "",
            state = "Notification",
        )
    }

}