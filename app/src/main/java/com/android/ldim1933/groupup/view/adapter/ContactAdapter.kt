package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Contact
import java.util.*
import kotlin.collections.ArrayList


class ContactAdapter(private var list: ArrayList<Contact>) :
    RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {
    private lateinit var mListener: OnItemClickListener

    private var originalList = ArrayList<Contact>()
    private var filteredList = ArrayList<Contact>()
    private var currentList = ArrayList<Contact>()

    init {
        originalList.addAll(list)
        filteredList = list.filter { contact -> contact.hasApplication } as ArrayList<Contact>
        currentList.addAll(list)
    }

    interface OnItemClickListener {
        fun onClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }

    inner class ContactViewHolder(itemView: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.contact_item_name)
        var number: TextView = itemView.findViewById(R.id.contact_item_number)
        var imageTxt: TextView = itemView.findViewById(R.id.contact_item_image)
        var hasAppImg: ImageView = itemView.findViewById(R.id.hasApplicationImage)

        init {
            itemView.setOnClickListener {
                listener.onClick(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false),
            mListener
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact: Contact = list[position]
        holder.name.text = contact.name
        holder.number.text = contact.telephoneNumber

        var senderFirstLetter = holder.name.text
        if (holder.name.text.length > 1) {
            senderFirstLetter = holder.name.text.subSequence(0, 2) as String
        }
        holder.imageTxt.text = senderFirstLetter
        holder.imageTxt.setBackgroundColor(getColor())

        if (contact.hasApplication) {
            holder.hasAppImg.visibility = View.VISIBLE
        } else {
            holder.hasAppImg.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun getColor(): Int {
        val randomBackgroundColor = Random()
        return Color.argb(
            255,
            randomBackgroundColor.nextInt(256),
            randomBackgroundColor.nextInt(256),
            randomBackgroundColor.nextInt(256)
        )
    }

    @SuppressLint("NotifyDataSetChanged")
    fun showContactsWithApplication() {
        list = filteredList
        currentList = filteredList
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun showAllContacts() {
        list = originalList
        currentList = originalList
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun filterList(filterList: ArrayList<Contact>) {
        list = filterList
        notifyDataSetChanged()
    }

    fun getContact(position: Int): Contact {
        return list[position]
    }

    fun getList(): MutableList<Contact> {
        return currentList
    }
}