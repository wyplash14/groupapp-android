package com.android.ldim1933.groupup.view.fragment.option

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.databinding.FragmentOptionsBinding

class OptionFragment : Fragment() {

    lateinit var binding: FragmentOptionsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOptionsBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.todoButton.setOnClickListener {
            findNavController().navigate(R.id.actionOptionsToToDo)
        }
        binding.contactBookButton.setOnClickListener {
            findNavController().navigate(R.id.actionOptionsToContacts)
        }

        binding.pollButton.setOnClickListener {
            findNavController().navigate(R.id.action_nav_menu_to_pollFragment)
        }

        binding.reminderButton.setOnClickListener {
            findNavController().navigate(R.id.action_nav_menu_to_notificationFragment)
        }
    }

}