package com.android.ldim1933.groupup.common

enum class PollType(val type: String) {
    MODIFY("modify"),
    NONMODIFY("nonmodify"),
}