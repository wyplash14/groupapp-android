package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.data.model.Poll
import com.android.ldim1933.groupup.data.model.PollPoint
import com.android.ldim1933.groupup.data.repository.PollRepository

class PollRepositoryViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = PollRepository(application)
    private val _pollLiveData = MutableLiveData<List<Poll>>()
    private var pollLiveData: LiveData<List<Poll>> = _pollLiveData

    fun create(poll: Poll) {
        repository.create(poll)
    }

    fun getUserPolls(uid: String, progressBar: ProgressBar? = null): LiveData<List<Poll>> {
        repository.getPollsOfUser(uid, _pollLiveData)
        if (progressBar != null) {
            progressBar.visibility = View.GONE
        }
        return pollLiveData
    }

    fun addNewPointToPoll(poll: Poll, votePoint: PollPoint) {
        repository.addNewPointToPoll(poll, votePoint)
    }

    fun addVoteToPollPoint(poll: Poll, pollPoint: PollPoint, uid: String) {
        repository.addVoteToPollPoint(poll, pollPoint, uid)
    }

    fun removeVoteFromPollPoint(poll: Poll, pollPoint: PollPoint, uid: String) {
        repository.removeVoteFromPollPoint(poll, pollPoint, uid)
    }

}