package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.data.model.NotificationData
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter(private val list: MutableList<NotificationData>) :
    RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {

    private lateinit var mListener: OnItemClickListener
    private var originalNotFilteredList: MutableList<NotificationData> = mutableListOf()

    init {
        originalNotFilteredList.addAll(list)
    }

    interface OnItemClickListener {
        fun onClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }

    class NotificationViewHolder(itemView: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView) {
        val message: TextView = itemView.findViewById(R.id.description_rem_txt)
        val date: TextView = itemView.findViewById(R.id.date_rem_txt)
        val hour: TextView = itemView.findViewById(R.id.hour_rem_txt)

        init {
            itemView.setOnClickListener {
                listener.onClick(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        return NotificationViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.notification_item, parent, false),
            mListener
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.message.text = list[position].message
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = list[position].time!!.toLong()
        holder.date.text = SimpleDateFormat(
            "yyyy/MM/dd",
            Locale.getDefault()
        ).format(list[position].time!!.toLong())
        holder.hour.text = "${calendar.get(Calendar.HOUR_OF_DAY)}:${calendar.get(Calendar.MINUTE)}"
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        list.clear()
    }

    fun setNotificationList(mutableList: MutableList<NotificationData>) {
        clear()
        list.addAll(mutableList)
        originalNotFilteredList.clear()
        originalNotFilteredList.addAll(mutableList)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setNotificationListOfGroup(group: Group) {
        list.clear()
        if (group.id.equals(Constants.ALL_GROUPS_ID)) {
            list.addAll(originalNotFilteredList)
        } else {
            list.addAll(originalNotFilteredList.filter { it.groupName == group.name && it.groupImage == group.profilePictureUrl })
        }
        notifyDataSetChanged()
    }
}