package com.android.ldim1933.groupup.view.adapter


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.DetailedTodoPopup
import com.android.ldim1933.groupup.common.TaskStateEnum
import com.android.ldim1933.groupup.data.model.NotificationData
import com.android.ldim1933.groupup.data.model.Poll
import com.android.ldim1933.groupup.data.model.Task
import com.android.ldim1933.groupup.viewmodel.TasksViewModel
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.ColorTemplate
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

class DashboardViewPagerAdapter(
    private var title: List<String>,
    private var tasks: MutableList<Task>,
    private val tasksViewModel: TasksViewModel,
    private val notifications: MutableList<NotificationData>,
    private val polls: MutableList<Poll>
) : RecyclerView.Adapter<DashboardViewPagerAdapter.Pager2ViewHolder>() {

    private val adapter: TaskAdapter = TaskAdapter(tasks, false)
    private val adapterNotification = NotificationAdapter(notifications)
    private val adapterPoll = PollAdapter(polls)

    inner class Pager2ViewHolder(itemView: View) : ViewHolder(itemView) {
        val itemTitle: TextView = itemView.findViewById(R.id.viewPagerTitle)
        val recyclerView: RecyclerView = itemView.findViewById(R.id.dashboardRecyclerView)
        val counterTxt: TextView = itemView.findViewById(R.id.numberThingTxt)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DashboardViewPagerAdapter.Pager2ViewHolder {
        return Pager2ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.todo_page_on_dashboard, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: DashboardViewPagerAdapter.Pager2ViewHolder,
        position: Int
    ) {
        holder.itemTitle.text = title[position]

        when (position) {
            0 -> {
                holder.recyclerView.adapter = adapter
                val itemTouchHelper = ItemTouchHelper(simpleCallback)
                itemTouchHelper.attachToRecyclerView(holder.recyclerView)
                holder.counterTxt.text = adapter.itemCount.toString()
                adapter.setOnItemClickListener(object : TaskAdapter.OnItemClickListener {
                    override fun onClick(position: Int) {
                        holder.itemView.context.let { it1 ->
                            DetailedTodoPopup(it1).showInPopup(
                                tasks[position]
                            )
                        }
                    }
                })
            }
            1 -> {
                holder.recyclerView.adapter = adapterNotification
                holder.counterTxt.text = adapterNotification.itemCount.toString()
                adapterNotification.setOnItemClickListener(object :
                    NotificationAdapter.OnItemClickListener {
                    override fun onClick(position: Int) {
                        holder.itemView.context.let { it1 ->
                            DetailedTodoPopup(it1).showInPopup(
                                convertNotificationToTask(notifications[position])
                            )
                        }
                    }
                })
            }
            else -> {
                holder.recyclerView.adapter = adapterPoll
                holder.counterTxt.text = adapterPoll.itemCount.toString()
                adapterPoll.setOnItemClickListener(object : PollAdapter.OnItemClickListener {
                    override fun onClick(position: Int) {
                        holder.itemView.context.let {
                            loadChartOfPoll(polls[position], holder)
                        }
                    }
                })
            }
        }
    }

    override fun getItemCount(): Int {
        return title.size
    }

    private val simpleCallback =
        object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: ViewHolder,
                target: ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                when (direction) {
                    ItemTouchHelper.LEFT -> {
                        setDialog(viewHolder.itemView.context, position)

                    }
                    else -> {
                        tasksViewModel.changeTaskState(
                            tasks[position].assignedToId!!,
                            tasks[position].id!!,
                            TaskStateEnum.DONE
                        )
                        adapter.changeState(position, TaskStateEnum.DONE)

                    }
                }
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                RecyclerViewSwipeDecorator.Builder(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
                    .addSwipeLeftBackgroundColor(
                        ContextCompat.getColor(
                            viewHolder.itemView.context, R.color.red_normal
                        )
                    )
                    .addSwipeLeftActionIcon(R.drawable.delete_icon)
                    .addSwipeRightBackgroundColor(
                        ContextCompat.getColor(viewHolder.itemView.context, R.color.green_light)
                    )
                    .addSwipeRightLabel(viewHolder.itemView.context.getString(R.string.done))
                    .setSwipeRightLabelTextSize(1, 20F)
                    .setSwipeRightLabelColor(
                        ContextCompat.getColor(
                            viewHolder.itemView.context,
                            R.color.white
                        )
                    )
                    .setSwipeRightLabelTypeface(Typeface.DEFAULT_BOLD)
                    .create()
                    .decorate()

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )

            }
        }

    @SuppressLint("NotifyDataSetChanged")
    private fun setDialog(context: Context, position: Int) {
        val dialogInvite = Dialog(context)
        dialogInvite.setContentView(R.layout.decline_todo_dialog)
        dialogInvite.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialogInvite.window?.attributes?.windowAnimations = R.style.animationDialog
        dialogInvite.setCancelable(false)

        dialogInvite.findViewById<TextView>(R.id.todoDescriptionTxtdecline).text =
            tasks[position].description

        dialogInvite.findViewById<Button>(R.id.btn_yes_todo).setOnClickListener {
            tasksViewModel.changeTaskState(
                tasks[position].assignedToId!!,
                tasks[position].id!!,
                TaskStateEnum.DECLINED
            )
            adapter.changeState(position, TaskStateEnum.DECLINED)
            dialogInvite.dismiss()
        }

        dialogInvite.findViewById<Button>(R.id.btn_no_todo).setOnClickListener {
            adapter.notifyDataSetChanged()
            dialogInvite.dismiss()
        }

        dialogInvite.show()
    }

    fun convertNotificationToTask(notification: NotificationData): Task {
        return Task(
            id = notification.id ?: "",
            assignedToId = notification.assignedToId ?: "",
            assignedById = notification.assignedById ?: "",
            groupName = notification.groupName,
            description = notification.message ?: "",
            groupImage = notification.groupImage,
            userName = notification.userName,
            userImage = notification.userImage,
            toUserImage = notification.toUserImage,
            toUserName = notification.toUserName,
            dueTime = notification.time ?: "",
            groupId = notification.groupId ?: "",
            state = "Notification",
        )
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addNotificationList(notificationList: MutableList<NotificationData>) {
        notifications.clear()
        notifications.addAll(notificationList)
        adapterNotification.setNotificationList(notificationList)
        notifyDataSetChanged()
    }

    fun addTodoList(taskList: MutableList<Task>) {
        tasks.clear()
        tasks.addAll(taskList)
        adapter.setTaskList(taskList)
        notifyItemRangeChanged(0, tasks.size)
    }

    fun addPollList(pollList: MutableList<Poll>) {
        polls.clear()
        polls.addAll(pollList)
        notifyItemRangeChanged(0, polls.size)
    }

    private fun loadChartOfPoll(poll: Poll, holder: Pager2ViewHolder) {
        val dialogBuilder = AlertDialog.Builder(holder.itemView.context)
        val chartPollView =
            LayoutInflater.from(holder.itemView.context).inflate(R.layout.popup_poll_chart, null)

        val infoTxt = chartPollView.findViewById<TextView>(R.id.chartItemTxt)
        val pieChart = chartPollView.findViewById<PieChart>(R.id.chartView)

        pieChart.isRotationEnabled = true
        pieChart.setUsePercentValues(true)
        pieChart.setHoleColor(R.color.blue_dark)
        pieChart.setCenterTextColor(Color.WHITE)
        pieChart.holeRadius = 40f
        pieChart.setTransparentCircleAlpha(0)
        pieChart.setDrawEntryLabels(true)
        pieChart.description.text = ""
        pieChart.setEntryLabelTextSize(18F)
        pieChart.setEntryLabelColor(Color.BLACK)
        pieChart.legend.isEnabled = false


        val entries = ArrayList<PieEntry>()
        poll.pollPoint.forEach {
            if (it.voters.size != 0) {
                entries.add(PieEntry(it.voters.size.toFloat() / 2, it.description.toString()))
            }
        }

        val colors = ArrayList<Int>()

        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)

        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS) colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS) colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)


        val dataSet = PieDataSet(entries, "")
        dataSet.colors = colors

        val data = PieData(dataSet)
        data.setDrawValues(true)
        data.setValueFormatter(PercentFormatter(pieChart))
        data.setValueTextSize(13f)
        data.setValueTextColor(Color.BLACK)
        pieChart.data = data
        pieChart.invalidate()

        pieChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            @SuppressLint("SetTextI18n")
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                Log.d("PieChart", e?.y.toString())
                Log.d("PieChart", (e as PieEntry).label)
                infoTxt.text = "At ${e.label} item, voted ${e.y.toInt()}"
                infoTxt.visibility = View.VISIBLE
            }

            override fun onNothingSelected() {
                infoTxt.visibility = View.GONE
            }

        })

        dialogBuilder.setView(chartPollView)
        val dialog = dialogBuilder.create()
        dialog.show()
    }

}