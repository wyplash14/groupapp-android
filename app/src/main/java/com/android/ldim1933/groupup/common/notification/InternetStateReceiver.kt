package com.android.ldim1933.groupup.common.notification

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.provider.Settings
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.view.MainActivity

class InternetStateReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            if (!isConnectedToInternet(context)) {
                showInternetConnectionDialog(context)
            }
        }
    }

    private fun showInternetConnectionDialog(context: Context) {
        val dialogInternet = Dialog(context)
        dialogInternet.setContentView(R.layout.internet_required_dialog)
        dialogInternet.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialogInternet.window?.attributes?.windowAnimations = R.style.animationDialog
        dialogInternet.setCancelable(false)

        dialogInternet.show()

        dialogInternet.findViewById<Button>(R.id.btn_connect).setOnClickListener {
            context.startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
        }

        dialogInternet.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }

    private fun isConnectedToInternet(context: Context): Boolean {
        val connectionManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifiConn = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val mobileConn = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)

        Log.i("INTERNET_CONNECTION", wifiConn.toString() + " " + mobileConn.toString())
        if ((wifiConn != null && wifiConn.isAvailable) || (mobileConn != null && mobileConn.isAvailable)) {
            return true
        }

        return false
    }
}