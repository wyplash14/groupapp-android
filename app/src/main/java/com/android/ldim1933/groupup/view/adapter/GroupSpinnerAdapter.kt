package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Group
import com.bumptech.glide.Glide


class GroupSpinnerAdapter(private val list: ArrayList<Group>) : BaseAdapter() {
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rootView =
            LayoutInflater.from(parent!!.context).inflate(R.layout.group_item_scroll, parent, false)
        val name: TextView = rootView.findViewById(R.id.group_item_name)
        val image: ImageView = rootView.findViewById(R.id.group_item_image)

        name.text = list[position].name

        if (list[position].profilePictureUrl != null) {
            Glide.with(image.context).load(list[position].profilePictureUrl)
                .into(image)
        }

        return rootView
    }
}