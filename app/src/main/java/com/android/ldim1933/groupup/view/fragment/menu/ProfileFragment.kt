package com.android.ldim1933.groupup.view.fragment.menu

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.User
import com.android.ldim1933.groupup.databinding.FragmentProfileBinding
import com.android.ldim1933.groupup.view.MainActivity
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class ProfileFragment : Fragment() {
    lateinit var binding: FragmentProfileBinding
    lateinit var userDataViewModel: UserRepositoryViewModel
    private var imageUri: Uri? = null
    private var user: User? = null
    private var isUpdated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)

        userDataViewModel.getUserData(Firebase.auth.currentUser!!.uid)
            .observe(this.viewLifecycleOwner, {
                binding.editTextFirstName.hint = it.firstName
                binding.editTextLastName.hint = it.lastName
                binding.editTextTelephoneNumber.text = it.telephoneNumber
                Glide.with(this).load(it.profilePictureUrl?.toUri())
                    .into(binding.profilePictureProfileImageView)
                binding.editTextEmail.text = Firebase.auth.currentUser!!.email

                user = User(
                    uid = it.uid,
                    firstName = it.firstName,
                    lastName = it.lastName,
                    telephoneNumber = it.telephoneNumber,
                    email = it.email,
                    profilePictureUrl = it.profilePictureUrl,
                    token = it.token,
                    groups = it.groups
                )
            })

        textChangeListeners()

        binding.saveProfileBtn.setOnClickListener {
            updateProfile()
        }

        binding.uploadProfilePicture.setOnClickListener {
            selectImage()
        }

        binding.changePasswordTxt.setOnClickListener {
            createRestorationDialog()
        }
        return binding.root
    }

    private fun createRestorationDialog() {
        val dialogBuilder = AlertDialog.Builder(this.context!!)
        val restorePopupView = layoutInflater.inflate(R.layout.reset_pw_dialog, null)
        val email: EditText = restorePopupView.findViewById(R.id.emailRegisterResetTxt)
        email.setText(binding.editTextEmail.text)

        restorePopupView.findViewById<Button>(R.id.btn_restoration).setOnClickListener {
            if (email.text.isEmpty()) {
                email.error = "Fill with your email."
                email.requestFocus()
            } else {
                Firebase.auth.sendPasswordResetEmail(email.text.toString()).addOnSuccessListener {
                    Toast.makeText(
                        this.context,
                        "Reset Link Sent To Your Email.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        dialogBuilder.setView(restorePopupView)
        val dialog = dialogBuilder.create()
        dialog.show()
    }

    private fun textChangeListeners() {
        binding.editTextFirstName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.saveProfileBtn.visibility = View.GONE
                    } else {
                        binding.saveProfileBtn.visibility = View.VISIBLE
                    }
                }
            }

        })

        binding.editTextLastName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.saveProfileBtn.visibility = View.GONE
                    } else {
                        binding.saveProfileBtn.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun updateProfile() {
        Log.i("Update Profile", user.toString())
        if (user != null) {
            if (binding.editTextFirstName.text.trim().toString().isNotEmpty()) {
                user!!.firstName = binding.editTextFirstName.text.trim().toString()
            }
            if (binding.editTextLastName.text.trim().toString().isNotEmpty()) {
                user!!.lastName = binding.editTextLastName.text.trim().toString()
            }
            userDataViewModel.create(user!!, imageUri)
            isUpdated = true
        }
    }

    private fun selectImage() {
        val intent = Intent()

        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == AppCompatActivity.RESULT_OK) {
            binding.profilePictureProfileImageView.setImageURI(data!!.data)
            imageUri = data.data!!
            binding.saveProfileBtn.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isUpdated) {
            (activity as MainActivity).loadUserData()
        }
    }

}