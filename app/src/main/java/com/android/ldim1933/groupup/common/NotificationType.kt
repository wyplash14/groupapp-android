package com.android.ldim1933.groupup.common

enum class NotificationType(val type:String) {
    NOTIFICATION("notification"),
    TODO("todo"),
    POLL("poll"),
    INVITATION("invitation")
}