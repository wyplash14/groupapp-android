package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Group
import com.bumptech.glide.Glide

class GroupsAdapter(private var list: MutableList<Group>) :
    RecyclerView.Adapter<GroupsAdapter.GroupViewHolder>() {

    private lateinit var mListener: OnItemClickListener
    private val originalList = list

    interface OnItemClickListener {
        fun onClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }

    fun getItem(position: Int): Group {
        return list[position]
    }

    fun getList(): MutableList<Group> {
        return originalList
    }

    fun clearList() {
        list.clear()
    }

    inner class GroupViewHolder(itemView: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.group_item_name)
        var count: TextView = itemView.findViewById(R.id.group_item_memberNum)
        var image: ImageView = itemView.findViewById(R.id.group_item_image)

        init {
            itemView.setOnClickListener {
                listener.onClick(adapterPosition)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        return GroupViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.group_item, parent, false),
            mListener
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        val group: Group = list[position]
        Log.i("GROUP ADAPTER", group.profilePictureUrl.toString())
        holder.name.text = group.name
        holder.count.text = group.members.size.toString() +" "+ holder.itemView.context.getString(R.string.members)
        if (group.profilePictureUrl != null) {
            Glide.with(holder.itemView.context).load(group.profilePictureUrl)
                .into(holder.image)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun filterList(filteredList: ArrayList<Group>) {
        list = filteredList
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addList(groupList: ArrayList<Group>) {
        list.clear()
        list.addAll(groupList)
        notifyDataSetChanged()
    }
}