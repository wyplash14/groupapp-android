package com.android.ldim1933.groupup.view.fragment.group

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.DetailedTodoPopup
import com.android.ldim1933.groupup.common.TaskStateEnum
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.databinding.FragmentGroupInfoBinding
import com.android.ldim1933.groupup.view.adapter.TaskAdapter
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.TasksViewModel
import com.bumptech.glide.Glide


class GroupInfoFragment : Fragment() {
    private lateinit var binding: FragmentGroupInfoBinding
    private val args: GroupInfoFragmentArgs by navArgs()
    private var imageUri: Uri? = null
    private lateinit var group: Group
    private lateinit var groupDataViewModel: GroupRepositoryViewModel
    private lateinit var taskViewModel: TasksViewModel
    private val taskAdapter = TaskAdapter(mutableListOf(), true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        group = args.group
        groupDataViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]
        taskViewModel = ViewModelProvider(this)[TasksViewModel::class.java]
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentGroupInfoBinding.inflate(inflater, container, false)

        binding.editTextTextGroupName.hint = group.name
        binding.editTextTextGroupDescription.hint = group.description

        if (group.profilePictureUrl != null) {
            Glide.with(this).load(group.profilePictureUrl)
                .into(binding.groupInfoImg)
        }

        binding.uploadImgIcon.setOnClickListener {
            selectImage()
        }

        binding.editTextTextGroupName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.saveGroupBtn.visibility = View.GONE
                    } else {
                        binding.saveGroupBtn.visibility = View.VISIBLE
                    }
                }
            }

        })

        binding.editTextTextGroupDescription.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.saveGroupBtn.visibility = View.GONE
                    } else {
                        binding.saveGroupBtn.visibility = View.VISIBLE
                    }
                }
            }

        })

        binding.saveGroupBtn.setOnClickListener {
            updateGroup()
        }

        binding.recyclerViewActivity.adapter = taskAdapter

        taskViewModel.getGroupTasks(group.id!!).observe(viewLifecycleOwner, { taskList ->
            taskAdapter.setTaskList(taskList.toMutableList())
            binding.doneTxt.text =
                "Done: ${taskList.filter { elem -> elem.state == TaskStateEnum.DONE.title }.size}"
            binding.pandingTxt.text =
                "Pending: ${taskList.filter { elem -> elem.state == TaskStateEnum.PENDING.title }.size}"
            binding.declinedTxt.text =
                "Declined: ${taskList.filter { elem -> elem.state == TaskStateEnum.DECLINED.title }.size}"

            binding.doneTxt.setOnClickListener {
                taskAdapter.setTaskList(taskList.filter { elem -> elem.state == TaskStateEnum.DONE.title }
                    .toMutableList())
            }

            binding.pandingTxt.setOnClickListener {
                taskAdapter.setTaskList(taskList.filter { elem -> elem.state == TaskStateEnum.PENDING.title }
                    .toMutableList())
            }

            binding.declinedTxt.setOnClickListener {
                taskAdapter.setTaskList(taskList.filter { elem -> elem.state == TaskStateEnum.DECLINED.title }
                    .toMutableList())
            }

            binding.activityTxt.setOnClickListener {
                taskAdapter.setTaskList(taskList.toMutableList())
            }
            taskAdapter.setOnItemClickListener(object : TaskAdapter.OnItemClickListener {
                @RequiresApi(Build.VERSION_CODES.N)
                override fun onClick(position: Int) {
                    this@GroupInfoFragment.context?.let { it1 ->
                        DetailedTodoPopup(it1).showInPopup(
                            taskList[position]
                        )
                    }
                }
            })
        })

        return binding.root
    }

    private fun updateGroup() {
        if (binding.editTextTextGroupName.text.trim().toString().isNotEmpty()) {
            group.name = binding.editTextTextGroupName.text.trim().toString()
        }
        if (binding.editTextTextGroupDescription.text.trim().toString().isNotEmpty()) {
            group.description = binding.editTextTextGroupDescription.text.trim().toString()
        }

        groupDataViewModel.create(group, imageUri)
        findNavController().navigate(R.id.action_groupInfoFragment_to_nav_group)

    }

    private fun selectImage() {
        val intent = Intent()

        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100 && resultCode == AppCompatActivity.RESULT_OK) {
            binding.groupInfoImg.setImageURI(data!!.data)
            imageUri = data.data!!
            binding.saveGroupBtn.visibility = View.VISIBLE
        }

    }
}