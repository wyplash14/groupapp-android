package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.data.model.Invitation
import com.android.ldim1933.groupup.data.repository.GroupInvitationRepository

class GroupInvitationViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = GroupInvitationRepository(application)

    fun create(invitation: Invitation) {
        repository.create(invitation)
    }

    fun getUserInvitation(userId: String): MutableLiveData<ArrayList<Invitation>> {
        val invitationData = MutableLiveData<ArrayList<Invitation>>()
        repository.getUsersInvitations(userId,invitationData)
        return invitationData
    }

    fun deleteInvitation(invitationId: String) {
        repository.deleteInvitation(invitationId)
    }

}