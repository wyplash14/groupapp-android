package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.Task
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class TaskRepository(private val application: Application) {
    private val database =
        Firebase.database("https://groupup-6ed7f-default-rtdb.europe-west1.firebasedatabase.app/")
    private val tData = database.getReference(Constants.TASKS)
    private val uData = database.getReference(Constants.USERS)
    private val gData = database.getReference(Constants.GROUPS)

    fun create(task: Task) {
        tData.child(task.assignedToId!!).child(task.id!!).setValue(task).addOnSuccessListener {
            Toast.makeText(
                application,
                application.getString(R.string.taskd),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun getUserTasks(id: String, liveData: MutableLiveData<List<Task>>) {
        val list = ArrayList<Task>()
        tData.child(id).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                Log.i("SNAPSHOT", snapshot.value.toString())
                snapshot.children.forEach { child ->
                    val task: HashMap<String, String> = child.value as HashMap<String, String>
                    val tId = task["id"]
                    val desc = task["description"]
                    val to = task["assignedToId"]
                    val by = task["assignedById"]
                    val groupId = task["groupId"]
                    val due = task["dueTime"]
                    val state = task["state"]

                    gData.child(groupId!!).get().addOnSuccessListener { group ->
                        val groupname = group.child("name").value.toString()
                        val groupImage = group.child("profilePictureUrl").value.toString()
                        uData.child(by!!).get()
                            .addOnSuccessListener { user ->
                                val byFirstname = user.child("firstName").value.toString()
                                val byLastname = user.child("lastName").value.toString()
                                val byImage = user.child("profilePictureUrl").value.toString()
                                uData.child(to!!).get().addOnSuccessListener { toUser ->
                                    val toFirstname = toUser.child("firstName").value.toString()
                                    val toLastname = toUser.child("lastName").value.toString()
                                    val toImage = toUser.child("profilePictureUrl").value.toString()

                                    list.add(
                                        Task(
                                            id = tId!!,
                                            description = desc!!,
                                            assignedById = by,
                                            assignedToId = to,
                                            dueTime = due!!,
                                            groupId = groupId,
                                            toUserName = "$toFirstname $toLastname",
                                            toUserImage = toImage,
                                            groupImage = groupImage,
                                            groupName = groupname,
                                            userImage = byImage,
                                            userName = "$byFirstname $byLastname",
                                            state = state!!
                                        )
                                    )
                                    liveData.postValue(list)
                                }
                            }
                    }
                }
                list.clear()
            }

            override fun onCancelled(error: DatabaseError) {
                //Nothing to do
            }

        })
    }

    fun getGivenTasks(uid: String, _tasksLiveData: MutableLiveData<List<Task>>) {
        val list = ArrayList<Task>()
        tData.ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach { child ->
                    Log.i("SNAPSHOT child:", child.value.toString())
                    tData.child(child.key.toString()).orderByChild("assignedById").equalTo(uid)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(it: DataSnapshot) {
                                it.children.forEach { element ->
                                    Log.i("SNAPSHOT ASSIGNED BY", element.value.toString())
                                    val task = element.value as HashMap<String, String>
                                    val id = task["id"]
                                    val desc = task["description"]
                                    val to = task["assignedToId"]
                                    val by = task["assignedById"]
                                    val group = task["groupId"]
                                    val due = task["dueTime"]
                                    val state = task["state"]

                                    gData.child(group!!).child("name").get()
                                        .addOnSuccessListener { groupName ->
                                            val groupname = groupName.value as String
                                            gData.child(group).child("profilePictureUrl").get()
                                                .addOnSuccessListener { groupImage ->
                                                    val groupim = groupImage.value as String
                                                    uData.child(by!!).child("profilePictureUrl")
                                                        .get()
                                                        .addOnSuccessListener { user ->
                                                            val userPicture = user.value as String
                                                            uData.child(to!!).get()
                                                                .addOnSuccessListener { toUser ->
                                                                    val toFirstname =
                                                                        toUser.child("firstName").value.toString()
                                                                    val toLastname =
                                                                        toUser.child("lastName").value.toString()
                                                                    val profilePictureUrl =
                                                                        toUser.child("profilePictureUrl").value.toString()
                                                                    list.add(
                                                                        Task(
                                                                            id = id!!,
                                                                            description = desc!!,
                                                                            assignedById = by,
                                                                            assignedToId = to,
                                                                            dueTime = due!!,
                                                                            groupId = group,
                                                                            groupName = groupname,
                                                                            userImage = userPicture,
                                                                            toUserImage = profilePictureUrl,
                                                                            toUserName = "$toFirstname $toLastname",
                                                                            groupImage = groupim,
                                                                            state = state!!
                                                                        )
                                                                    )

                                                                    _tasksLiveData.postValue(list)
                                                                }
                                                        }
                                                }
                                        }
                                }

                            }

                            override fun onCancelled(error: DatabaseError) {
                                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
                            }
                        })
                }
                list.clear()
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }

        })
    }

    fun changeTaskState(uid: String, taskId: String, state: String) {
        tData.child(uid).child(taskId).child("state").setValue(state).addOnSuccessListener {
            Toast.makeText(application, application.getString(R.string.taskstate), Toast.LENGTH_LONG).show()
        }
    }

    fun getGroupTasks(groupId: String, _tasksLiveData: MutableLiveData<List<Task>>) {
        val taskList = ArrayList<Task>()
        gData.child(groupId).child("members").get().addOnSuccessListener { members ->
            members.children.forEach { groupIdDS ->
                tData.child(groupIdDS.value.toString()).orderByChild("groupId").equalTo(groupId)
                    .get().addOnSuccessListener { ds ->
                        ds.children.forEach {
                            val task = it.getValue(Task::class.java)
                            if (task != null) {
                                gData.child(groupId).get().addOnSuccessListener { group ->
                                    val groupName = group.child("name").value.toString()
                                    val groupImage =
                                        group.child("profilePictureUrl").value.toString()
                                    uData.child(task.assignedById!!).get()
                                        .addOnSuccessListener { byUser ->
                                            val byFirstname =
                                                byUser.child("firstName").value.toString()
                                            val byLastname =
                                                byUser.child("lastName").value.toString()
                                            val profilePictureUrlBy =
                                                byUser.child("profilePictureUrl").value.toString()
                                            uData.child(task.assignedToId!!).get()
                                                .addOnSuccessListener { toUser ->
                                                    val toFirstname =
                                                        toUser.child("firstName").value.toString()
                                                    val toLastname =
                                                        toUser.child("lastName").value.toString()
                                                    val profilePictureUrl =
                                                        toUser.child("profilePictureUrl").value.toString()

                                                    task.userName = "$byFirstname $byLastname"
                                                    task.userImage = profilePictureUrlBy
                                                    task.toUserName = "$toFirstname $toLastname"
                                                    task.toUserImage = profilePictureUrl
                                                    task.groupName = groupName
                                                    task.groupImage = groupImage
                                                    taskList.add(task)
                                                    Log.i("TASK", task.toString())
                                                    _tasksLiveData.postValue(taskList)
                                                }
                                        }
                                }
                            }
                        }

                    }

            }
        }
    }
}

