package com.android.ldim1933.groupup.view

import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.net.toUri
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.notification.InternetStateReceiver
import com.android.ldim1933.groupup.common.notification.NotificationService
import com.android.ldim1933.groupup.databinding.ActivityMainBinding
import com.android.ldim1933.groupup.viewmodel.AuthViewModel
import com.android.ldim1933.groupup.viewmodel.GroupInvitationViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.bumptech.glide.Glide
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: AuthViewModel
    private lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var invitationViewModel: GroupInvitationViewModel
    private lateinit var header: View

    private val internetStateReceiver = InternetStateReceiver()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel = ViewModelProvider(this)[AuthViewModel::class.java]
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        invitationViewModel = ViewModelProvider(this)[GroupInvitationViewModel::class.java]

        loadUserData()

        val sharedPrefferences = getSharedPreferences("night", 0)
        val isNightModeOn = sharedPrefferences.getBoolean("night_mode", false)
        if (isNightModeOn) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        val sharedLanguage = getSharedPreferences("language",0)
        val language = sharedLanguage.getString("language","en")
        setLanguage(language!!)

        val bottomNavigationView: BottomNavigationView = binding.bottomNav
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHostFragmentActivityMain) as NavHostFragment
        val navController = navHostFragment.findNavController()

        bottomNavigationView.setupWithNavController(navController)

        val drawerLayout = binding.drawerLayout
        val userImageNav = binding.userImageNav

        userImageNav.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }

        header = binding.navigationUserMenu.getHeaderView(0)
        header.findViewById<Button>(R.id.logoutBtnHeader).setOnClickListener {
            viewModel.signOut()
            openAuthActivity()
            Toast.makeText(this, "Logged out", Toast.LENGTH_LONG).show()
            finish()
        }

        binding.navigationUserMenu.menu.findItem(R.id.nav_profile).setOnMenuItemClickListener {
            navController.navigate(R.id.open_Profile_Fragment_action)
            drawerLayout.closeDrawers()
            super.onOptionsItemSelected(it)
        }

        val originalTitle =  binding.navigationUserMenu.menu.findItem(R.id.nav_mygroups).title

        invitationViewModel.getUserInvitation(Firebase.auth.currentUser!!.uid).observe(this, {
            binding.navigationUserMenu.menu.findItem(R.id.nav_mygroups).title = "$originalTitle ${it.size}"
        })

        binding.navigationUserMenu.menu.findItem(R.id.nav_mygroups).setOnMenuItemClickListener {
            navController.navigate(R.id.open_Group_invitation_action)
            drawerLayout.closeDrawers()
            super.onOptionsItemSelected(it)
        }

        binding.navigationUserMenu.menu.findItem(R.id.nav_settings).setOnMenuItemClickListener {
            navController.navigate(R.id.open_Settings_action)
            drawerLayout.closeDrawers()
            super.onOptionsItemSelected(it)
        }

        navController.addOnDestinationChangedListener { _, navDestination: NavDestination, _ ->
            if (navDestination.id == R.id.todoFragment || navDestination.id == R.id.contactFragment || navDestination.id == R.id.pollFragment ||
                navDestination.id == R.id.notificationFragment || navDestination.id == R.id.groupFragment || navDestination.id == R.id.groupInfoFragment ||
                navDestination.id == R.id.groupMembersFragment || navDestination.id == R.id.groupAdminsFragment || navDestination.id == R.id.profileFragment
                || navDestination.id == R.id.invitationFragment || navDestination.id == R.id.settingsFragment
            ) {
                bottomNavigationView.visibility = View.GONE
                userImageNav.visibility = View.GONE
            } else {
                bottomNavigationView.visibility = View.VISIBLE
                userImageNav.visibility = View.VISIBLE
            }
        }

    }

    @SuppressLint("SetTextI18n")
    fun loadUserData() {
        if (Firebase.auth.currentUser != null) {
            userDataViewModel.getUserData(Firebase.auth.currentUser!!.uid)
                .observe(this, { user ->
                    header.findViewById<TextView>(R.id.userNameTxt).text =
                        "${user.firstName} ${user.lastName}"
                    header.findViewById<TextView>(R.id.telNumTxt).text = "${user.telephoneNumber}"
                    if (user.profilePictureUrl != null) {
                        Log.i("Profile picture", user.profilePictureUrl.toString())
                        Glide.with(this).load(user.profilePictureUrl?.toUri())
                            .into(header.findViewById<CircleImageView>(R.id.menuHeaderProfilePicture))
                        Glide.with(this).load(user.profilePictureUrl?.toUri())
                            .into(binding.userImageNav)
                    }
                })
            updateToken(Firebase.auth.currentUser!!.uid)
        } else {
            Log.i("MAIN","Launch Auth")
            openAuthActivity()
            this.finish()
        }
    }

    private fun openAuthActivity() {
        val intent = Intent(this, AuthActivity::class.java)
        startActivity(intent)
    }


    private fun updateToken(uid: String) {
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            userDataViewModel.updateToken(uid, it)
            NotificationService.token = it
        }

    }

    override fun onStart() {
        registerReceiver(
            internetStateReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        super.onStart()
    }

    override fun onStop() {
        unregisterReceiver(internetStateReceiver)
        super.onStop()
    }

    private fun setLanguage(language: String) {
        val resources = resources
        val metrics = resources.displayMetrics
        val configuration = resources.configuration
        configuration.locale = Locale(language)
        resources.updateConfiguration(configuration,metrics)
        onConfigurationChanged(configuration)
    }

}