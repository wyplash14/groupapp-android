package com.android.ldim1933.groupup.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class Group(
    var id: String? = null,
    var name: String? = null,
    var description: String? = null,
    var profilePictureUrl: String? = null,
    var createdBy: String? = null,
    var admins: ArrayList<String> = arrayListOf(),
    var members: ArrayList<String> = arrayListOf(),
) : Parcelable