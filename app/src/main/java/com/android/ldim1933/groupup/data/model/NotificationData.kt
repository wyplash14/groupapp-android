package com.android.ldim1933.groupup.data.model

data class NotificationData(
    val id: String? = null,
    val groupId: String? = null,
    val assignedToId: String? = null,
    val assignedById: String? = null,
    val title: String? = null,
    val message: String? = null,
    val time: String? = null,
    var isScheduled: String? = null,
    val type: String? = null,

    var groupName: String? = null,
    var userName: String? = null,
    var userImage: String? = null,
    var toUserName: String? = null,
    var toUserImage: String? = null,
    var groupImage: String? = null,
)
