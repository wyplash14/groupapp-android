package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class GroupRepository(private val application: Application) {

    private val database =
        Firebase.database("https://groupup-6ed7f-default-rtdb.europe-west1.firebasedatabase.app/")
    private val gData = database.getReference(Constants.GROUPS)
    private val tData = database.getReference(Constants.TASKS)
    private val pData = database.getReference(Constants.POLLS)
    private val uData = database.getReference(Constants.USERS)
    private val nData = database.getReference(Constants.NOTIFICATIONS)
    private val cData = database.getReference(Constants.CONTACTS)
    private val iData = database.getReference(Constants.INVITATIONS)

    private val imageUploader = ImageUploadRepository(gData)
    private val userRepository = UserRepository(application)

    fun create(group: Group, image: Uri?) {
        group.id?.let { it ->
            gData.child(it).setValue(group).addOnSuccessListener {
                Toast.makeText(
                    application,
                    application.getString(R.string.groupdata),
                    Toast.LENGTH_LONG
                ).show()
                userRepository.addGroupToUser(group.createdBy!!, group.id!!)
                if (image != null) {
                    imageUploader.uploadPicture(group.id!!, image)
                }
            }.addOnFailureListener { e ->
                Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }
        }
    }

    fun addMemberToGroup(groupid: String, userid: String) {
        gData.child(groupid).get().addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result!!.exists()) {

                    val group = it.result!!.getValue(Group::class.java)!!
                    gData.child(groupid).child("members").child(group.members.size.toString())
                        .setValue(userid).addOnSuccessListener {
                            userRepository.addGroupToUser(userid, groupid)
                            Toast.makeText(
                                application,
                                application.getString(R.string.memberg),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                }
            }
        }
    }

    fun getGroupById(id: String, groupData: MutableLiveData<Group>) {
        gData.child(id).get().addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result!!.exists()) {
                    groupData.postValue(it.result!!.getValue(Group::class.java))
                }
            }
        }.addOnFailureListener {
            Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
        }
    }

    fun getGroupsById(
        groupIdList: ArrayList<String>,
        groupsData: MutableLiveData<ArrayList<Group>>
    ) {
        val listOfGroups = ArrayList<Group>()
        for (groupId in groupIdList) {
            gData.child(groupId).get().addOnCompleteListener {
                if (it.result!!.exists()) {
                    listOfGroups.add(it.result!!.getValue(Group::class.java)!!)
                    groupsData.postValue(listOfGroups)
                }
            }
        }
    }

    fun addNewAdmin(groupId: String, uid: String) {
        gData.child(groupId).get().addOnSuccessListener {
            val group = it.getValue(Group::class.java)
            group?.admins?.add(uid)
            gData.child(groupId).setValue(group).addOnFailureListener {
                Log.e("ADD ADMIN", it.message.toString())
            }
        }
    }

    fun deleteGroup(id: String) {
        gData.child(id).get().addOnSuccessListener {
            val group = it.getValue(Group::class.java)
            group!!.members.forEach { member ->
                uData.child(member).child("groups").get().addOnSuccessListener {
                    val groups: ArrayList<String> = it.value as ArrayList<String>
                    groups.remove(id)
                    uData.child(member).child("groups").setValue(groups).addOnFailureListener {
                        Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        gData.child(id).removeValue().addOnFailureListener {
            Toast.makeText(application, application.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
        }

        tData.get().addOnSuccessListener {
            it.children.forEach {
                it.children.forEach { node ->
                    val task = node.getValue(Task::class.java)
                    if (task!!.groupId == id) {
                        node.ref.removeValue()
                    }
                }
            }
        }

        //Delete group polls
        pData.get().addOnSuccessListener {
            it.children.forEach {
                val poll = it.getValue(Poll::class.java)
                if (poll!!.groupId == id) {
                    it.ref.removeValue()
                }
            }
        }

        //Delete group contacts
        cData.child(id).removeValue()

        //Delete group notifications
        nData.get().addOnSuccessListener {
            it.children.forEach {
                it.children.forEach { node ->
                    val not = node.getValue(NotificationData::class.java)
                    if (not!!.groupId == id) {
                        node.ref.removeValue()
                    }
                }
            }
        }

        //Delete group invitations
        iData.get().addOnSuccessListener {
            it.children.forEach {
                val invitation = it.getValue(Invitation::class.java)
                if (invitation!!.invitedTo == id) {
                    it.ref.removeValue()
                }
            }
        }

    }

    fun addNewAdminAndRemoveAnother(groupId: String, userIdAdd: String, userIdRemove: String) {
        gData.child(groupId).get().addOnSuccessListener {
            val group = it.getValue(Group::class.java)
            group?.admins?.add(userIdAdd)
            group?.admins?.remove(userIdRemove)
            group?.members?.remove(userIdRemove)
            gData.child(groupId).setValue(group).addOnFailureListener {
                Log.e("ADD ADMIN", it.message.toString())
            }
        }
    }


}