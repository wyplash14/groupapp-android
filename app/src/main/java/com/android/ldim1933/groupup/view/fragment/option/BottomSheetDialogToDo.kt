package com.android.ldim1933.groupup.view.fragment.option

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.IdGenerator
import com.android.ldim1933.groupup.common.NotificationType
import com.android.ldim1933.groupup.common.TaskStateEnum
import com.android.ldim1933.groupup.common.notification.SendNotification
import com.android.ldim1933.groupup.data.model.NotificationData
import com.android.ldim1933.groupup.data.model.PushNotification
import com.android.ldim1933.groupup.data.model.Task
import com.android.ldim1933.groupup.data.model.User
import com.android.ldim1933.groupup.databinding.PopupTodoBinding
import com.android.ldim1933.groupup.view.adapter.*
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.TasksViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.*


class BottomSheetDialogToDo : BottomSheetDialogFragment(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    lateinit var binding: PopupTodoBinding
    private lateinit var adapterGroup: GroupSpinnerAdapter
    private lateinit var adapterMember: UserSpinnerAdapter
    private lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var groupDataViewModel: GroupRepositoryViewModel
    private lateinit var tasksViewModel: TasksViewModel

    private var groupId: String? = null
    private var userId: String? = null
    private var userToken: String? = null

    private val cal = Calendar.getInstance()
    private val day = cal.get(Calendar.DAY_OF_MONTH)
    private val month = cal.get(Calendar.MONTH)
    private val year = cal.get(Calendar.YEAR)
    private val hour = cal.get(Calendar.HOUR)
    private val minute = cal.get(Calendar.MINUTE)

    private val currentDate = "$year/$month/$day - $hour:$minute"

    private var savedDay = day
    private var savedMonth = month
    private var savedYear = year
    private var savedHour = hour
    private var savedMinute = minute

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        groupDataViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]
        tasksViewModel = ViewModelProvider(this)[TasksViewModel::class.java]
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PopupTodoBinding.inflate(inflater, container, false)

        userDataViewModel.getUserGroupsById(Firebase.auth.currentUser!!.uid)
            .observe(this.viewLifecycleOwner, {
                groupDataViewModel.getGroupsDataByIds(it)
                    .observe(this.viewLifecycleOwner, { groups ->
                        adapterGroup = GroupSpinnerAdapter(groups)
                        binding.spinnerTodoGrpup.adapter = adapterGroup
                        binding.spinnerTodoGrpup.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    groupId = groups[position].id!!
                                    userDataViewModel.getUsersById(groups[position].members)
                                        .observe(this@BottomSheetDialogToDo.viewLifecycleOwner,
                                            { users ->
                                                adapterMember =
                                                    UserSpinnerAdapter(users as ArrayList<User>)
                                                binding.spinnerTodoUser.adapter = adapterMember
                                                binding.spinnerTodoUser.onItemSelectedListener =
                                                    object : AdapterView.OnItemSelectedListener {
                                                        override fun onItemSelected(
                                                            parent: AdapterView<*>?,
                                                            view: View?,
                                                            position: Int,
                                                            id: Long
                                                        ) {
                                                            userId = users[position].uid
                                                            userToken = users[position].token
                                                        }

                                                        override fun onNothingSelected(parent: AdapterView<*>?) {
                                                            TODO("Not yet implemented")
                                                        }

                                                    }

                                            })
                                }

                            }
                    })
            })

        binding.todoTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isNotEmpty()) {
                        binding.todoTxt.setBackgroundResource(R.drawable.add_todo_bg_light)
                    } else {
                        binding.todoTxt.setBackgroundResource(R.drawable.add_todo_bg)
                    }
                    checkAllFilled()
                }
            }

        })

        binding.editTextDate.hint = currentDate

        binding.editTextDate.setOnClickListener {
            selectDateAndHour()
        }

        binding.todoCreateButton.setOnClickListener {
            addTodo()
            this.dismiss()
        }

        return binding.root
    }

    private fun addTodo() {
        if (binding.todoTxt.text.trim().length > 255) {
            binding.todoTxt.error =
                "Text size should be max 255 characters. Current size is: ${binding.todoTxt.text.trim().length}."
            binding.todoTxt.requestFocus()
        } else {
            if (year>savedYear || month>savedMonth || day>savedDay) {
                binding.editTextDate.error = "You should pick a date from the future."
                binding.editTextDate.requestFocus()
            } else {
                Toast.makeText(this.context, "Adding todo to db!Successful", Toast.LENGTH_LONG)
                    .show()
                tasksViewModel.create(
                    Task(
                        id = IdGenerator.generate(userId!!, "task", ""),
                        assignedToId = userId!!,
                        assignedById = Firebase.auth.currentUser!!.uid,
                        description = binding.todoTxt.text.trim().toString(),
                        dueTime = binding.editTextDate.text.toString(),
                        groupId = groupId!!,
                        state = TaskStateEnum.PENDING.title
                    )
                )
                notificationSend()
            }
        }
    }

    private fun notificationSend() {
        PushNotification(
            NotificationData(
                title = "GroupUp",
                message = binding.todoTxt.text.trim().toString(),
                isScheduled = "false",
                time = "",
                type = NotificationType.TODO.type
            ),
            userToken!!
        ).also {
            Log.i("Notification item:", it.toString())
            SendNotification.sendNotification(it)
        }
    }

    private fun checkAllFilled() {
        if (binding.todoTxt.text.trim().toString().isNotEmpty() && binding.editTextDate.text.trim()
                .toString().isNotEmpty() && groupId != null && userId != null
        ) {
            binding.todoCreateButton.visibility = View.VISIBLE
        } else {
            binding.todoCreateButton.visibility = View.GONE
        }
    }

    private fun selectDateAndHour() {
        DatePickerDialog(this.context!!, this, year, month, day).show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month
        savedYear = year
        TimePickerDialog(this.context, this, hour, minute, true).show()
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        savedHour = hourOfDay
        savedMinute = minute
        binding.editTextDate.text = "$savedYear/$savedMonth/$savedDay - $savedHour:$savedMinute"
        binding.editTextDate.setBackgroundResource(R.drawable.add_todo_bg_light)
        checkAllFilled()
    }
}