package com.android.ldim1933.groupup.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.common.TaskStateEnum
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.data.model.Task
import com.bumptech.glide.Glide

class TaskAdapter(private val list: MutableList<Task>, private val showPerson: Boolean) :
    RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {
    private lateinit var mListener: OnItemClickListener

    var originalNotFilteredList: MutableList<Task> = mutableListOf()

    init {
        originalNotFilteredList.addAll(list)
    }

    interface OnItemClickListener {
        fun onClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }


    class TaskViewHolder(itemView: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView) {
        val groupName: TextView = itemView.findViewById(R.id.group_todo_name)
        val description: TextView = itemView.findViewById(R.id.description_todo_txt)
        val date: TextView = itemView.findViewById(R.id.date_todo_txt)
        val image: ImageView = itemView.findViewById(R.id.dashboard_person_image)
        val toImage: ImageView = itemView.findViewById(R.id.dashboard_person_do_image)
        val backgroundLayout: ConstraintLayout = itemView.findViewById(R.id.item_todo_base)

        init {
            itemView.setOnClickListener {
                listener.onClick(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.task_item, parent, false),
            mListener
        )
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.groupName.text = list[position].groupName
        holder.date.text = list[position].dueTime
        holder.description.text = list[position].description
        if (list[position].userImage != null) {
            Glide.with(holder.itemView.context).load(list[position].userImage)
                .into(holder.image)
        }

        when (list[position].state) {
            TaskStateEnum.DONE.title -> holder.backgroundLayout.background =
                ContextCompat.getColor(holder.itemView.context, R.color.green_light).toDrawable()
            TaskStateEnum.PENDING.title -> holder.backgroundLayout.background =
                ContextCompat.getColor(holder.itemView.context, R.color.blue_light).toDrawable()
            TaskStateEnum.DECLINED.title -> holder.backgroundLayout.background =
                ContextCompat.getColor(holder.itemView.context, R.color.red_normal).toDrawable()
        }

        if (showPerson) {
            holder.toImage.visibility = View.VISIBLE
            if (list[position].userImage != null) {
                Glide.with(holder.itemView.context).load(list[position].toUserImage)
                    .into(holder.toImage)
            }
        } else {
            holder.toImage.visibility = View.GONE
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        list.clear()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun changeState(position: Int, stateEnum: TaskStateEnum) {
        originalNotFilteredList[position].state = stateEnum.title
        list.clear()
        list.addAll(originalNotFilteredList.filter { it.state == TaskStateEnum.PENDING.title } as MutableList<Task>)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setTaskList(mutableList: MutableList<Task>) {
        clear()
        list.addAll(mutableList)
        originalNotFilteredList.clear()
        originalNotFilteredList.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setTaskListOfGroup(group: Group) {
        list.clear()
        if (group.id.equals(Constants.ALL_GROUPS_ID)) {
            list.addAll(originalNotFilteredList)
        } else {
            list.addAll(originalNotFilteredList.filter { it.groupName == group.name && it.groupImage == group.profilePictureUrl })
        }
        notifyDataSetChanged()
    }

}