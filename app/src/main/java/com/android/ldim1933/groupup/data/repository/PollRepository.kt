package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.data.model.Poll
import com.android.ldim1933.groupup.data.model.PollPoint
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class PollRepository(private val application: Application) {

    private val database =
        Firebase.database("https://groupup-6ed7f-default-rtdb.europe-west1.firebasedatabase.app/")
    private val pData = database.getReference(Constants.POLLS)
    private val uData = database.getReference(Constants.USERS)
    private val gData = database.getReference(Constants.GROUPS)

    fun create(poll: Poll) {
        pData.child(poll.id!!).setValue(poll).addOnSuccessListener {
            Toast.makeText(
                application,
                application.getString(R.string.polld),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun getPollsOfUser(uid: String, mutableLiveData: MutableLiveData<List<Poll>>) {
        val list = ArrayList<Poll>()
        uData.child(uid).child("groups").get().addOnSuccessListener { groupIds ->
            groupIds.children.forEach { groupId ->
                pData.orderByChild("groupId").equalTo(groupId.value.toString()).get()
                    .addOnSuccessListener { polls ->
                        polls.children.forEach { poll ->
                            val pollResult = poll.getValue(Poll::class.java)!!
                            pollResult.creatorId?.let {
                                uData.child(it).get().addOnSuccessListener { creator ->
                                    val firstname = creator.child("firstName").value.toString()
                                    val lastname = creator.child("lastName").value.toString()
                                    val userImage =
                                        creator.child("profilePictureUrl").value.toString()

                                    gData.child(groupId.value.toString()).get()
                                        .addOnSuccessListener { groupData ->
                                            val groupName = groupData.child("name").value.toString()
                                            val groupPicture =
                                                groupData.child("profilePictureUrl").value.toString()
                                            list.add(
                                                Poll(
                                                    id = pollResult.id,
                                                    creatorId = pollResult.creatorId,
                                                    groupId = groupId.value.toString(),
                                                    title = pollResult.title,
                                                    description = pollResult.description,
                                                    type = pollResult.type,
                                                    creatorUserName = "$firstname $lastname",
                                                    creatorUserImage = userImage,
                                                    groupName = groupName,
                                                    groupImage = groupPicture,
                                                    pollPoint = pollResult.pollPoint
                                                )
                                            )
                                            mutableLiveData.postValue(list)
                                        }
                                }
                            }
                        }
                    }
            }
        }
        list.clear()
    }

    fun addNewPointToPoll(poll: Poll, pollPoint: PollPoint) {
        pData.child(poll.id!!).get().addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result!!.exists()) {
                    Log.i("POLL", it.result.toString())
                    val pollResult = it.result!!.getValue(Poll::class.java)!!
                    pData.child(poll.id).child("pollPoint")
                        .child(pollResult.pollPoint.size.toString())
                        .setValue(pollPoint).addOnSuccessListener {
                            Toast.makeText(
                                application,
                                application.getString(R.string.pollp),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                }
            }

        }
    }

    fun addVoteToPollPoint(poll: Poll, pollPoint: PollPoint, uid: String) {
        pData.child(poll.id!!).child("pollPoint").get().addOnSuccessListener {
            it.children.forEach { pp ->
                val point = pp.getValue(PollPoint::class.java)
                if (point!!.pid == pollPoint.pid && !point.voters.contains(uid)) {
                    pollPoint.voters.add(uid)
                    pData.child(poll.id).child("pollPoint").child(pp.key.toString())
                        .setValue(pollPoint)
                }
            }
        }
    }

    fun removeVoteFromPollPoint(poll: Poll, pollPoint: PollPoint, uid: String) {
        pData.child(poll.id!!).child("pollPoint").get().addOnSuccessListener {
            it.children.forEach { pp ->
                val point = pp.getValue(PollPoint::class.java)
                if (point!!.pid == pollPoint.pid && point.voters.contains(uid)) {
                    pollPoint.voters.remove(uid)
                    pData.child(poll.id).child("pollPoint").child(pp.key.toString())
                        .setValue(pollPoint)
                }
            }
        }
    }

}