package com.android.ldim1933.groupup.data.model

data class Invitation(
    //used for db
    var invitationId: String? = null,
    var invitedBy: String? = null,
    var invitedUser: String? = null,
    var invitedTo: String? = null,

    //used in ui
    var createdOn: String? = null,
    var groupName: String? = null,
    var invitedByName: String? = null,
    var invitedByPictureUrl: String? = null,
)
