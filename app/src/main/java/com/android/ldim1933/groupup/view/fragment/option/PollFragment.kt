package com.android.ldim1933.groupup.view.fragment.option

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.common.IdGenerator
import com.android.ldim1933.groupup.common.NotificationType
import com.android.ldim1933.groupup.common.PollType
import com.android.ldim1933.groupup.common.notification.SendNotification
import com.android.ldim1933.groupup.data.model.*
import com.android.ldim1933.groupup.databinding.FragmentPollBinding
import com.android.ldim1933.groupup.view.adapter.GroupMemberAdapter
import com.android.ldim1933.groupup.view.adapter.GroupSpinnerAdapter2
import com.android.ldim1933.groupup.view.adapter.PollAdapter
import com.android.ldim1933.groupup.view.adapter.PollPointAdapter
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.PollRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class PollFragment : Fragment() {
    private lateinit var binding: FragmentPollBinding
    private lateinit var pollViewModel: PollRepositoryViewModel
    private lateinit var userRepositoryViewModel: UserRepositoryViewModel
    private lateinit var groupRepositoryViewModel: GroupRepositoryViewModel
    private lateinit var adapter: PollAdapter

    var poz = 0
    private var detailedPoz = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pollViewModel = ViewModelProvider(this)[PollRepositoryViewModel::class.java]
        userRepositoryViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        groupRepositoryViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]

        detailedPoz = this.arguments?.getInt("Poll") ?: 0
        adapter = PollAdapter(mutableListOf())
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPollBinding.inflate(inflater, container, false)

        binding.pollLayout.setOnRefreshListener {
            val fragment = PollFragment()
            val args = Bundle()
            args.putInt("Poll", poz)
            fragment.arguments = args
            fragmentManager!!.beginTransaction().replace(this.id, fragment).commit()
            binding.pollLayout.isRefreshing = false
        }

        binding.addPollButton.setOnClickListener {
            val bs = BottomSheetDialogPoll()
            bs.show(childFragmentManager, "Add")
        }

        Firebase.auth.uid?.let {
            pollViewModel.getUserPolls(it).observe(viewLifecycleOwner,
                { polls ->
                    Log.i("POLLS", polls.toString())
                    adapter.addPollList(polls as MutableList<Poll>)
                    adapter.notifyDataSetChanged()
                    binding.pollRecyclerView.layoutManager =
                        LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
                    binding.pollRecyclerView.adapter = adapter
                    adapter.setOnItemClickListener(object : PollAdapter.OnItemClickListener {
                        override fun onClick(position: Int) {
                            loadDetailedPoll(adapter.getPolls()[position])
                            poz = position
                        }
                    })

                    val groupList = mutableSetOf<Group>()
                    groupList.add(Group(name = Constants.ALL_GROUPS, id = Constants.ALL_GROUPS_ID))
                    polls.forEach { poll ->
                        groupList.add(
                            Group(
                                name = poll.groupName,
                                profilePictureUrl = poll.groupImage
                            )
                        )
                    }
                    binding.spinnerPollGroup.adapter =
                        GroupSpinnerAdapter2(groupList.toMutableList() as ArrayList<Group>)

                    binding.spinnerPollGroup.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                adapter.setPollsOfGroup(groupList.toMutableList()[position])

                            }
                        }
                })
        }

        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun loadDetailedPoll(poll: Poll) {
        binding.pollDetailedCardContainer.pollDetailedCard.visibility = View.VISIBLE
        binding.pollDetailedCardContainer.pollItemName.text = poll.title
        binding.pollDetailedCardContainer.pollItemCreatorName.text =
            "Created by: ${poll.creatorUserName}"
        binding.pollDetailedCardContainer.pollItemDescription.text = poll.description
        binding.pollDetailedCardContainer.pollItemGroupName.text = "Group: ${poll.groupName}"

        binding.pollDetailedCardContainer.pollItemVotes.text =
            "${poll.pollPoint.size} items to vote"

        if (poll.creatorId != Firebase.auth.currentUser!!.uid && poll.type == PollType.NONMODIFY.type) {
            binding.pollDetailedCardContainer.pollAddVoteEditText.visibility = View.GONE
        }

        groupRepositoryViewModel.getGroupData(poll.groupId!!).observe(viewLifecycleOwner, { group ->
            userRepositoryViewModel.getUsersById(group.members)
                .observe(viewLifecycleOwner, { groupUsers ->
                    binding.pollDetailedCardContainer.pollPointRecycleView.adapter =
                        PollPointAdapter(
                            poll.pollPoint as MutableList<PollPoint>,
                            poll,
                            pollViewModel,
                            groupUsers
                        )
                })
        })


        binding.pollDetailedCardContainer.pollAddVotePointButton.setOnClickListener {
            addNewPointToPoll(poll)
        }


        binding.pollDetailedCardContainer.pollAddVoteEditText.addTextChangedListener(object :
            TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isNotEmpty()) {
                        binding.pollDetailedCardContainer.pollAddVotePointButton.visibility =
                            View.VISIBLE
                    } else {
                        binding.pollDetailedCardContainer.pollAddVotePointButton.visibility =
                            View.GONE
                    }

                }
            }

        })

        binding.pollDetailedCardContainer.chartImg.setOnClickListener {
            loadChartOfPoll(poll)
        }

        binding.pollDetailedCardContainer.noUserImg.setOnClickListener {
            listMembersNotVoted(poll)
        }

        binding.pollDetailedCardContainer.notificationImg.setOnClickListener {
            sendNotificationNotVoters(poll)
        }


    }

    private fun sendNotificationNotVoters(poll: Poll) {
        val dialogInvite = Dialog(this.context!!)
        dialogInvite.setContentView(R.layout.notification_poll_dialog)
        dialogInvite.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialogInvite.window?.attributes?.windowAnimations = R.style.animationDialog
        dialogInvite.setCancelable(false)


        dialogInvite.findViewById<Button>(R.id.btn_notify).setOnClickListener {
            sendNotificationsPeople(poll)
            dialogInvite.dismiss()
        }

        dialogInvite.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            dialogInvite.dismiss()
        }

        dialogInvite.show()
    }

    private fun sendNotificationsPeople(poll: Poll) {
        val votedList = ArrayList<String>()
        val notVotedList = ArrayList<String>()
        poll.pollPoint.forEach { pollPoint ->
            votedList.addAll(pollPoint.voters)
        }

        groupRepositoryViewModel.getGroupData(poll.groupId.toString())
            .observe(viewLifecycleOwner, { group ->
                group.members.forEach { member ->
                    if (!votedList.contains(member) && !notVotedList.contains(member)) {
                        notVotedList.add(member)
                        userRepositoryViewModel.getUserData(member)
                            .observe(viewLifecycleOwner, {
                                SendNotification.sendNotification(
                                    PushNotification(
                                        NotificationData(
                                            title = "GroupUp",
                                            message = "It's time to vote in poll: ${poll.title}",
                                            isScheduled = "false",
                                            time = "",
                                            type = NotificationType.POLL.type

                                        ), it.token.toString()
                                    )
                                )
                            })
                    }
                }
            })
    }

    private fun listMembersNotVoted(poll: Poll) {
        val dialogBuilder = AlertDialog.Builder(this.context!!)
        val userPollView = layoutInflater.inflate(R.layout.user_list_popup, null)

        val infoTxt = userPollView.findViewById<TextView>(R.id.everyUserVotedTxt)
        val recyclerView = userPollView.findViewById<RecyclerView>(R.id.notVotedRecyclerView)

        val votedList = ArrayList<String>()
        val notVotedList = mutableSetOf<String>()
        poll.pollPoint.forEach { pollPoint ->
            votedList.addAll(pollPoint.voters)
        }

        infoTxt.visibility = View.VISIBLE
        val userAdapter = GroupMemberAdapter(mutableListOf())
        recyclerView.adapter = userAdapter

        groupRepositoryViewModel.getGroupData(poll.groupId.toString())
            .observe(viewLifecycleOwner, { group ->
                group.members.forEach { member ->
                    if (!votedList.contains(member)) {
                        notVotedList.add(member)
                    }
                }
                if (notVotedList.isNotEmpty()) {
                    infoTxt.visibility = View.GONE
                    userRepositoryViewModel.getUsersById(notVotedList.toMutableList() as ArrayList<String>)
                        .observe(viewLifecycleOwner, { notVotedUsers ->
                            userAdapter.addList(notVotedUsers.toMutableList())
                        })
                }
            })

        dialogBuilder.setView(userPollView)
        val dialog = dialogBuilder.create()
        dialog.show()
    }

    private fun loadChartOfPoll(poll: Poll) {
        val dialogBuilder = AlertDialog.Builder(this.context!!)
        val chartPollView = layoutInflater.inflate(R.layout.popup_poll_chart, null)

        val infoTxt = chartPollView.findViewById<TextView>(R.id.chartItemTxt)
        val pieChart = chartPollView.findViewById<PieChart>(R.id.chartView)

        pieChart.isRotationEnabled = true
        pieChart.setUsePercentValues(true)
        pieChart.setHoleColor(R.color.blue_dark)
        pieChart.setCenterTextColor(Color.WHITE)
        pieChart.holeRadius = 40f
        pieChart.setTransparentCircleAlpha(0)
        pieChart.setDrawEntryLabels(true)
        pieChart.description.text = ""
        pieChart.setEntryLabelTextSize(18F)
        pieChart.setEntryLabelColor(Color.BLACK)
        pieChart.legend.isEnabled = false


        val entries = ArrayList<PieEntry>()
        poll.pollPoint.forEach {
            if (it.voters.size != 0) {
                entries.add(PieEntry(it.voters.size.toFloat() / 2, it.description.toString()))
            }
        }

        val colors = ArrayList<Int>()

        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)

        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS) colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS) colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)


        val dataSet = PieDataSet(entries, "")
        dataSet.colors = colors

        val data = PieData(dataSet)
        data.setDrawValues(true)
        data.setValueFormatter(PercentFormatter(pieChart))
        data.setValueTextSize(13f)
        data.setValueTextColor(Color.BLACK)
        pieChart.data = data
        pieChart.invalidate()

        pieChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            @SuppressLint("SetTextI18n")
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                Log.d("PieChart", e?.y.toString())
                Log.d("PieChart", (e as PieEntry).label)
                infoTxt.text = "At ${e.label} item, voted ${e.y.toInt()}"
                infoTxt.visibility = View.VISIBLE
            }

            override fun onNothingSelected() {
                infoTxt.visibility = View.GONE
            }

        })

        dialogBuilder.setView(chartPollView)
        val dialog = dialogBuilder.create()
        dialog.show()
    }


    private fun addNewPointToPoll(poll: Poll) {
        val pollPoint = PollPoint(
            pid = IdGenerator.generate(poll.id!!, "point", ""),
            description = binding.pollDetailedCardContainer.pollAddVoteEditText.text.toString()
                .trim()
        )
        pollViewModel.addNewPointToPoll(poll, pollPoint)
        pollViewModel.getUserPolls(Firebase.auth.uid.toString())
            .observe(viewLifecycleOwner, { polls ->
                if (binding.pollDetailedCardContainer.pollDetailedCard.visibility == View.VISIBLE) {
                    loadDetailedPoll(
                        polls[binding.pollDetailedCardContainer.pollDetailedId.text.toString()
                            .toInt()]
                    )
                }
            })
    }

}