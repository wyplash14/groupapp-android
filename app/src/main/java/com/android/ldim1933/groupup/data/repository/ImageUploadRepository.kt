package com.android.ldim1933.groupup.data.repository

import android.net.Uri
import com.android.ldim1933.groupup.common.Constants
import com.google.firebase.database.DatabaseReference
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.text.SimpleDateFormat
import java.util.*

class ImageUploadRepository(private val mData: DatabaseReference) {
    private val mStorage = Firebase.storage.getReference(Constants.UPLOADS)

    fun uploadPicture(id: String, image: Uri) {
        val fileName = uploadImageFileName()
        mStorage.child(fileName).putFile(image)
            .addOnSuccessListener {
                mStorage.child(fileName).downloadUrl.addOnSuccessListener { uri ->
                    mData.child(id).child("profilePictureUrl").setValue(uri.toString())
                }
            }
    }

    private fun uploadImageFileName(): String {
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        return formatter.format(now)
    }
}