package com.android.ldim1933.groupup.view.fragment.option

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants
import com.android.ldim1933.groupup.common.DetailedTodoPopup
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.data.model.Task
import com.android.ldim1933.groupup.databinding.FragmentTodoBinding
import com.android.ldim1933.groupup.view.adapter.GroupSpinnerAdapter2
import com.android.ldim1933.groupup.view.adapter.TaskAdapter
import com.android.ldim1933.groupup.viewmodel.TasksViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class TodoFragment : Fragment() {

    lateinit var binding: FragmentTodoBinding
    private lateinit var taskViewModel: TasksViewModel
    private lateinit var taskMutableList: MutableList<Task>
    private lateinit var adapter: TaskAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        taskViewModel = ViewModelProvider(this)[TasksViewModel::class.java]
        taskMutableList = emptyArray<Task>().toMutableList()
        adapter = TaskAdapter(taskMutableList, true)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTodoBinding.inflate(inflater, container, false)

        binding.addToDoButton.setOnClickListener {
            val bs = BottomSheetDialogToDo()
            bs.show(childFragmentManager, "Add")
        }

        binding.todoRecyclerView.adapter = adapter

        Firebase.auth.currentUser?.let { user ->
            taskViewModel.getGivenTasks(user.uid).observe(viewLifecycleOwner, { taskList ->
                adapter.clear()
                adapter.setTaskList(taskList as MutableList<Task>)
                binding.todoRecyclerView.adapter = adapter


                adapter.setOnItemClickListener(object : TaskAdapter.OnItemClickListener {
                    @RequiresApi(Build.VERSION_CODES.N)
                    override fun onClick(position: Int) {
                        this@TodoFragment.context?.let { it1 ->
                            DetailedTodoPopup(it1).showInPopup(
                                taskList[position]
                            )
                        }
                    }

                })

                val groupList = mutableSetOf<Group>()
                groupList.add(Group(name = getString(R.string.all_groups), id = Constants.ALL_GROUPS_ID))
                taskList.forEach {
                    groupList.add(
                        Group(
                            name = it.groupName,
                            profilePictureUrl = it.groupImage
                        )
                    )
                }
                binding.spinnerTodoGroup.adapter =
                    GroupSpinnerAdapter2(groupList.toMutableList() as ArrayList<Group>)

                binding.spinnerTodoGroup.onItemSelectedListener =
                    object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {

                        }

                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View?,
                            position: Int,
                            id: Long
                        ) {
                            adapter.setTaskListOfGroup(groupList.toMutableList()[position])
                        }
                    }
            })
        }
        return binding.root
    }
}
