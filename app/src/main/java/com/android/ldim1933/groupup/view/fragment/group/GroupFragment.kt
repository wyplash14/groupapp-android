package com.android.ldim1933.groupup.view.fragment.group

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.IdGenerator
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.databinding.FragmentGroupsBinding
import com.android.ldim1933.groupup.view.adapter.GroupsAdapter
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.*
import kotlin.collections.ArrayList

class GroupFragment : Fragment() {

    private lateinit var binding: FragmentGroupsBinding
    private lateinit var groupDataViewModel: GroupRepositoryViewModel
    private lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var addGroupPopupView: View
    private var imageUri: Uri? = null
    private lateinit var adapter: GroupsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        groupDataViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]

    }

    @SuppressLint("NotifyDataSetChanged", "SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentGroupsBinding.inflate(inflater, container, false)

        adapter = GroupsAdapter(mutableListOf())
        userDataViewModel.getUserGroupsById(Firebase.auth.uid!!)
            .observe(viewLifecycleOwner, { groupIdList ->
                groupDataViewModel.getGroupsDataByIds(groupIdList).observe(viewLifecycleOwner, {
                    adapter.addList(it)
                    binding.groupsNumberTxt.text = "${getString(R.string.group_number)} ${adapter.itemCount}"
                    binding.recyclerViewGroup.adapter = adapter
                    adapter.setOnItemClickListener(object : GroupsAdapter.OnItemClickListener {
                        override fun onClick(position: Int) {
                            val action =
                                GroupFragmentDirections.actionGroupsToGroup(adapter.getItem(position))
                            findNavController().navigate(action)
                        }
                    })

                    binding.searchGroupTxt.addTextChangedListener(object : TextWatcher {
                        override fun beforeTextChanged(
                            s: CharSequence?,
                            start: Int,
                            count: Int,
                            after: Int
                        ) {
                        }

                        override fun onTextChanged(
                            s: CharSequence?,
                            start: Int,
                            before: Int,
                            count: Int
                        ) {
                        }

                        override fun afterTextChanged(s: Editable?) {
                            filter(s.toString())
                            Log.i("FILTER", "text has changed")
                        }

                    })

                })
            })

        return binding.root
    }

    private fun filter(text: String) {
        val filteredList = ArrayList<Group>()
        adapter.getList().forEach { group ->
            if (group.name?.lowercase(Locale.getDefault())?.contains(text) == true) {
                filteredList.add(group)
            }
        }
        adapter.filterList(filteredList)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.addGroupButton.setOnClickListener { createAddGroupDialog() }

    }


    @SuppressLint("InflateParams")
    private fun createAddGroupDialog() {
        val dialogBuilder = this.context?.let { AlertDialog.Builder(it) }
        addGroupPopupView = layoutInflater.inflate(R.layout.group_add_popup, null)


        dialogBuilder?.setView(addGroupPopupView)
        val dialog = dialogBuilder?.create()
        dialog?.show()

        addGroupPopupView.findViewById<ImageView>(R.id.groupImageUpload).setOnClickListener {
            selectImage()
        }

        addGroupPopupView.findViewById<Button>(R.id.groupCreateBtn).setOnClickListener {
            dialog?.dismiss()

            val name =
                addGroupPopupView.findViewById<EditText>(R.id.groupAddNameTxt).text.toString()
                    .trim()
            val description =
                addGroupPopupView.findViewById<EditText>(R.id.groupAddDescriptionTxt).text.toString()
                    .trim()

            userDataViewModel.getUserData(Firebase.auth.currentUser!!.uid)
                .observe(viewLifecycleOwner, { user ->
                    val group = Group()
                    group.id = IdGenerator.generate(user.uid!!, "group", name)
                    group.admins.add(user.uid!!)
                    group.name = name
                    group.description = description
                    group.members.add(user.uid!!)
                    group.createdBy = user.uid!!

                    groupDataViewModel.create(group, imageUri)
                })
        }
    }

    private fun selectImage() {
        val intent = Intent()

        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(intent, 90)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 90 && resultCode == AppCompatActivity.RESULT_OK) {
            addGroupPopupView.findViewById<ImageView>(R.id.groupImageUpload)
                .setImageURI(data!!.data)
            addGroupPopupView.findViewById<ImageView>(R.id.uploadGroupPictureSchema).visibility =
                View.VISIBLE
            imageUri = data.data!!
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter.clearList()
    }


}