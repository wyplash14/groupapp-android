package com.android.ldim1933.groupup.common.notification

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.NotificationType
import com.android.ldim1933.groupup.view.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlin.random.Random


private const val CHANNEL_ID = "my_channel"

class NotificationService : FirebaseMessagingService() {

    companion object {
        var token: String? = null
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val destination = when (message.data["type"]) {
            NotificationType.NOTIFICATION.type -> R.id.notificationFragment
            NotificationType.TODO.type -> R.id.mainActivity
            NotificationType.INVITATION.type -> R.id.invitationFragment
            else -> R.id.mainActivity
        }

        val intent = Intent(this, MainActivity::class.java)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationID = Random.nextInt()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(notificationManager, CHANNEL_ID)
            createNotificationChannel(notificationManager, "alarm")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = NavDeepLinkBuilder(this)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.navigation_graph)
            .setDestination(destination)
            .createPendingIntent()

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(message.data["title"])
            .setContentText(message.data["message"])
            .setSmallIcon(R.drawable.notification_icon)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .build()

        if (message.data["isScheduled"].toBoolean()) {
            Log.i("Do I received anything?", message.data.toString())
            setAlarm(message)
        } else {
            notificationManager.notify(notificationID, notification)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(
        notificationManager: NotificationManager,
        channelId: String
    ) {
        val channelName = "channelName"
        val channel =
            NotificationChannel(
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                description = "My channel description"
                enableLights(true)
                lightColor = Color.GREEN
            }
        notificationManager.createNotificationChannel(channel)
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        token = newToken
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    fun setAlarm(message: RemoteMessage) {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, AlarmReceiver::class.java)
        intent.putExtra("Title", message.data["title"])
        intent.putExtra("Message", message.data["message"])

        val pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)
        alarmManager.set(AlarmManager.RTC_WAKEUP, message.data["time"]!!.toLong(), pendingIntent)

        Log.i("ALARM SET", "Alarm set successfully ${message.data["message"]}")
    }
}

