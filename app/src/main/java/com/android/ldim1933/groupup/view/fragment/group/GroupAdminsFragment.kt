package com.android.ldim1933.groupup.view.fragment.group

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.User
import com.android.ldim1933.groupup.databinding.FragmentGroupAdminsBinding
import com.android.ldim1933.groupup.view.adapter.GroupMemberAdapter
import com.android.ldim1933.groupup.view.adapter.UserSpinnerAdapter
import com.android.ldim1933.groupup.viewmodel.GroupRepositoryViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class GroupAdminsFragment : Fragment() {
    lateinit var binding: FragmentGroupAdminsBinding

    private lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var groupDataViewModel: GroupRepositoryViewModel
    private val args: GroupAdminsFragmentArgs by navArgs()
    private lateinit var adapter: GroupMemberAdapter
    private var uid: String? = null
    private var name: String? = null
    private lateinit var dialogInvite: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        groupDataViewModel = ViewModelProvider(this)[GroupRepositoryViewModel::class.java]

        createDialog()
    }

    @SuppressLint("SetTextI18n")
    private fun createDialog() {
        dialogInvite = this.context?.let { Dialog(it) }!!
        dialogInvite.setContentView(R.layout.invitation_dialog)
        dialogInvite.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialogInvite.window?.attributes?.windowAnimations = R.style.animationDialog
        dialogInvite.setCancelable(false)

        dialogInvite.findViewById<Button>(R.id.btn_invite).text = "Yes"
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGroupAdminsBinding.inflate(inflater, container, false)
        adapter = GroupMemberAdapter(arrayListOf())
        binding.adminSpinner.adapter = UserSpinnerAdapter(arrayListOf())

        if (args.group.admins.contains(Firebase.auth.currentUser!!.uid)) {
            binding.adminSpinner.visibility = View.VISIBLE
            binding.addAdminButton.visibility = View.VISIBLE
            binding.adminTxt.visibility = View.VISIBLE
        } else {
            binding.adminSpinner.visibility = View.GONE
            binding.addAdminButton.visibility = View.GONE
            binding.adminTxt.visibility = View.GONE
        }

        userDataViewModel.getUsersById(args.group.admins).observe(this, {
            adapter = GroupMemberAdapter(it as MutableList<User>)
            binding.memberRecyclerView.adapter = adapter
        })

        userDataViewModel.getUsersById(args.group.members).observe(this, {
            binding.adminSpinner.adapter = UserSpinnerAdapter(it as ArrayList<User>)

            binding.adminSpinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        uid = it[position].uid
                        name = it[position].firstName + " " + it[position].lastName
                    }
                }
        })

        binding.addAdminButton.setOnClickListener {
            dialogInvite.findViewById<TextView>(R.id.textViewInvite).text =
                "Do you want to promote $name to admin?"
            dialogInvite.show()

        }

        dialogInvite.findViewById<Button>(R.id.btn_invite).setOnClickListener {
            if (args.group.admins.contains(uid)) {
                Toast.makeText(this.context, "This user already is an admin.", Toast.LENGTH_LONG)
                    .show()
            } else {
                if (args.group.id != null && uid != null) {
                    groupDataViewModel.addNewAdmin(args.group.id!!, uid!!)
                    Toast.makeText(this.context, "$name promoted to admin.", Toast.LENGTH_LONG)
                        .show()

                } else {
                    Toast.makeText(
                        this.context,
                        "Some error occurred try again later!",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
            }
            dialogInvite.dismiss()
        }

        dialogInvite.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            dialogInvite.dismiss()
        }

        return binding.root
    }
}