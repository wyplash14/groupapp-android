package com.android.ldim1933.groupup.common

import android.annotation.SuppressLint
import android.content.Context
import android.icu.util.Calendar
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toDrawable
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.Task
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*

class DetailedTodoPopup(private val context: Context) {

    @SuppressLint("SetTextI18n")
    fun showInPopup(task: Task) {
        val dialogBuilder = context.let { AlertDialog.Builder(it) }
        val detailedTodoPopupView =
            LayoutInflater.from(context).inflate(R.layout.detailed_task_popup, null)

        val todo = detailedTodoPopupView.findViewById<TextView>(R.id.detailed_task_description)
        val assignedBy = detailedTodoPopupView.findViewById<TextView>(R.id.detailed_task_assignedBy)
        val assignedByImg =
            detailedTodoPopupView.findViewById<ImageView>(R.id.detailed_task_assignedByImg)
        val assignedTo = detailedTodoPopupView.findViewById<TextView>(R.id.detailed_task_assignedTo)
        val assignedToImg =
            detailedTodoPopupView.findViewById<ImageView>(R.id.detailed_task_assignedToImg)
        val group = detailedTodoPopupView.findViewById<TextView>(R.id.detailed_task_groupName)
        val groupImg = detailedTodoPopupView.findViewById<ImageView>(R.id.detailed_task_groupImg)
        val dueDate = detailedTodoPopupView.findViewById<TextView>(R.id.detailed_task_dueDate)
        val state = detailedTodoPopupView.findViewById<TextView>(R.id.detailed_task_state)
        val progressBar =
            detailedTodoPopupView.findViewById<ProgressBar>(R.id.notification_progressbar)

        todo.text = task.description
        if (task.userName.isNullOrEmpty()) {
            assignedBy.text = "Self"
        } else {
            assignedBy.text = task.userName
        }

        if (task.toUserName.isNullOrEmpty()) {
            assignedTo.text = "Self"
        } else {
            assignedTo.text = task.toUserName
        }

        if (task.userImage != null) {
            this.context.let {
                Glide.with(it).load(task.userImage)
                    .into(assignedByImg)
            }
        }

        if (task.toUserImage != null) {
            this.context.let {
                Glide.with(it).load(task.toUserImage)
                    .into(assignedToImg)
            }
        }

        group.text = task.groupName
        if (task.groupImage != null) {
            this.context.let {
                Glide.with(it).load(task.groupImage)
                    .into(groupImg)
            }
        }

        dueDate.text = "Due: ${task.dueTime}"


        when (task.state) {
            TaskStateEnum.DECLINED.title -> {
                state.text = task.state
                state.background =
                    ContextCompat.getColor(this.context, R.color.red_normal).toDrawable()
            }
            TaskStateEnum.PENDING.title -> {
                state.text = task.state
                state.background =
                    ContextCompat.getColor(this.context, R.color.blue_light).toDrawable()
            }
            TaskStateEnum.DONE.title -> {
                state.text = task.state
                state.background =
                    ContextCompat.getColor(this.context, R.color.green_light).toDrawable()
            }
            "Notification" -> {
                state.background =
                    ContextCompat.getColor(this.context, R.color.blue_dark).toDrawable()
                progressBar.max = task.dueTime!!.toLong().toInt()
                progressBar.progress = Calendar.getInstance().timeInMillis.toInt()
                progressBar.visibility = View.VISIBLE
                dueDate.text = "Notify on: " + SimpleDateFormat(
                    "yyyy/MM/dd",
                    Locale.getDefault()
                ).format(task.dueTime.toLong())
            }

        }

        dialogBuilder.setView(detailedTodoPopupView)
        val dialog = dialogBuilder.create()
        dialog.show()
    }
}