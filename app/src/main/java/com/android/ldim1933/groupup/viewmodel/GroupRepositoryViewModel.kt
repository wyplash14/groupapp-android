package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.data.model.Group
import com.android.ldim1933.groupup.data.repository.GroupRepository

class GroupRepositoryViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = GroupRepository(application)

    fun create(group: Group, imageUri: Uri?) {
        repository.create(group, imageUri)
    }

    fun getGroupData(groupId: String): MutableLiveData<Group> {
        val groupData = MutableLiveData<Group>()
        repository.getGroupById(groupId, groupData)
        return groupData
    }

    fun getGroupsDataByIds(groupIdList: ArrayList<String>): MutableLiveData<ArrayList<Group>> {
        val groupsData = MutableLiveData<ArrayList<Group>>()
        repository.getGroupsById(groupIdList, groupsData)
        return groupsData
    }

    fun addUserToGroup(groupId: String, userId: String) {
        repository.addMemberToGroup(groupId, userId)
    }

    fun addNewAdmin(groupId: String, uid: String) {
        repository.addNewAdmin(groupId, uid)
    }

    fun addNewAdminAndRemoveAnother(groupId: String, userIdAdd: String, userIdRemove: String) {
        repository.addNewAdminAndRemoveAnother(groupId, userIdAdd, userIdRemove)
    }

    fun deleteGroup(id: String) {
        repository.deleteGroup(id)
    }

}