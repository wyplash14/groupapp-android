package com.android.ldim1933.groupup.common

object Constants {
    //Realtime firebase database nodes
    const val USERS = "Users"
    const val UPLOADS = "Uploads"
    const val GROUPS = "Groups"
    const val INVITATIONS = "Invitations"
    const val TASKS = "Tasks"
    const val POLLS = "Polls"
    const val CONTACTS = "Contacts"
    const val NOTIFICATIONS = "Notifications"

    //Filtering options
    const val ALL_GROUPS = "All groups"
    const val ALL_GROUPS_ID = "allGroupsId"

    //Notification request
    const val BASE_URL = "https://fcm.googleapis.com"
    const val SERVER_KEY =
        "AAAAGNINgtM:APA91bGOf2s7_-DUHHSD57gQCOt9Fogpd-alWnCK6wsK1Z3Sd8wdWjgwzsKjPSTA-msU_CSmcCCltODVYs2YJziXVpqGl-sNsRqfIXRI9IW1hd2SU6GlS92LgWhHsQQXIvlWFJhLAoh8"
    const val CONTENT_TYPE = "application/json"

    //Regex
    const val PASSWORD_PATTERN =
        "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^!&+=])(?=\\S+$).{4,}$"
}