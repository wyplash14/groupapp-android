package com.android.ldim1933.groupup.view.fragment.group

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ContentResolver
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.IdGenerator
import com.android.ldim1933.groupup.common.NotificationType
import com.android.ldim1933.groupup.common.notification.SendNotification
import com.android.ldim1933.groupup.data.model.*
import com.android.ldim1933.groupup.databinding.PopupInviteMembersBinding
import com.android.ldim1933.groupup.view.adapter.ContactAdapter
import com.android.ldim1933.groupup.viewmodel.GroupInvitationViewModel
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class BottomSheetDialogContact : BottomSheetDialogFragment() {

    private lateinit var adapter: ContactAdapter
    private lateinit var binding: PopupInviteMembersBinding
    private var contactList = ArrayList<Contact>()
    private lateinit var contactSet: MutableSet<Contact>
    private lateinit var userDataViewModel: UserRepositoryViewModel
    private lateinit var dialogInvite: Dialog
    private lateinit var groupInvitationViewModel: GroupInvitationViewModel
    private var invitation: Invitation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]
        groupInvitationViewModel = ViewModelProvider(this)[GroupInvitationViewModel::class.java]
        dialogInvite = this.context?.let { Dialog(it) }!!
        dialogInvite.setContentView(R.layout.invitation_dialog)
        dialogInvite.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialogInvite.window?.attributes?.windowAnimations = R.style.animationDialog
        dialogInvite.setCancelable(false)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PopupInviteMembersBinding.inflate(inflater, container, false)

        contactSet = mutableSetOf()
        getContacts()
        contactList.addAll(contactSet)

        userDataViewModel.getContactHasApplicationByPhoneNumber(contactList)
            .observe(viewLifecycleOwner,
                { listOfModifiedContacts ->
                    adapter = ContactAdapter(listOfModifiedContacts)
                    binding.contactRecycleView.adapter = adapter

                    binding.switchContact.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            adapter.showContactsWithApplication()
                        } else {
                            adapter.showAllContacts()
                        }
                    }

                    adapter.setOnItemClickListener(object : ContactAdapter.OnItemClickListener {
                        @RequiresApi(Build.VERSION_CODES.O)
                        @SuppressLint("SetTextI18n")
                        override fun onClick(position: Int) {
                            val textView: TextView = dialogInvite.findViewById(R.id.textViewInvite)
                            textView.text = "Invite ${adapter.getContact(position).name} to ${
                                arguments?.getParcelable<Group>("group")?.name
                            }"
                            val simpleDateFormat =
                                SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                            val date: String = simpleDateFormat.format(Date())
                            if (adapter.getContact(position).hasApplication) {
                                if (!inThisGroup(adapter.getContact(position))) {
                                    invitation = Invitation(
                                        invitationId = IdGenerator.generate(
                                            adapter.getContact(position).name!!,
                                            "invitation",
                                            arguments?.getParcelable<Group>("group")?.name!!
                                        ),
                                        invitedBy = Firebase.auth.currentUser?.uid!!,
                                        invitedUser = adapter.getContact(position).userId!!,
                                        invitedTo = arguments?.getParcelable<Group>("group")?.id!!,
                                        createdOn = date
                                    )
                                    dialogInvite.show()
                                } else {
                                    Toast.makeText(
                                        this@BottomSheetDialogContact.context,
                                        "This user already in your group.",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    invitation = null
                                }
                            } else {
                                Toast.makeText(
                                    this@BottomSheetDialogContact.context,
                                    "This user doesn't has the application.",
                                    Toast.LENGTH_LONG
                                ).show()
                                invitation = null
                            }

                        }
                    })

                })

        dialogInvite.findViewById<Button>(R.id.btn_invite).setOnClickListener {
            if (invitation != null) {
                groupInvitationViewModel.create(invitation!!)
                Toast.makeText(this.context, "Invitation sent successfully!", Toast.LENGTH_LONG)
                    .show()
                userDataViewModel.getUserData(invitation!!.invitedUser!!)
                    .observe(viewLifecycleOwner, { user ->
                        PushNotification(
                            NotificationData(
                                title = "GroupUp",
                                message = "You are invited in ${arguments?.getParcelable<Group>("group")?.name} group.",
                                isScheduled = "false",
                                time = "",
                                type = NotificationType.INVITATION.type,
                            ),
                            user.token!!
                        ).also {
                            SendNotification.sendNotification(it)
                        }
                    })
            }
            dialogInvite.dismiss()
        }

        dialogInvite.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            dialogInvite.dismiss()
        }

        binding.searchContactTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                filter(s.toString())
                Log.i("FILTER", "text has changed")
            }

        })


        return binding.root
    }


    private fun inThisGroup(contact: Contact): Boolean {
        return arguments?.getParcelable<Group>("group")?.members!!.contains(contact.userId)
    }


    private fun filter(text: String) {
        val filteredList = ArrayList<Contact>()
        adapter.getList().forEach { contact ->
            if (contact.name?.lowercase(Locale.getDefault())?.contains(text) == true) {
                filteredList.add(contact)
            }
        }
        adapter.filterList(filteredList)
    }

    @SuppressLint("Range")
    private fun getContacts() {
        if (ContextCompat.checkSelfPermission(
                binding.root.context,
                android.Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            this.activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf(android.Manifest.permission.READ_CONTACTS), 0
                )
            }
        }

        val contentResolver: ContentResolver = this.activity!!.contentResolver
        val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val cursor = contentResolver.query(uri, null, null, null, null)

        if (cursor != null) {
            if (cursor.count > 0) {
                while (cursor.moveToNext()) {
                    val contactName =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                    val contactNumber =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER))
                    val contactPhoto =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                   if (contactNumber!=null){
                       contactSet.add(
                           Contact(
                               name = contactName,
                               image = contactPhoto,
                               telephoneNumber = contactNumber
                           )
                       )
                   }

                }
            }
        }
        cursor?.close()
    }
}