package com.android.ldim1933.groupup.data.model

data class Poll(
    val id: String? = null,
    val groupId: String? = null,
    val creatorId: String? = null,
    val title: String? = null,
    val description: String? = null,
    var type: String? = null,

    var pollPoint: ArrayList<PollPoint> = arrayListOf(),
    var groupName: String? = null,
    var groupImage: String? = null,
    var creatorUserName: String? = null,
    var creatorUserImage: String? = null,
)



