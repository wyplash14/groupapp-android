package com.android.ldim1933.groupup.view.fragment.group

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.data.model.User
import com.android.ldim1933.groupup.databinding.FragmentGroupMembersBinding
import com.android.ldim1933.groupup.view.adapter.GroupMemberAdapter
import com.android.ldim1933.groupup.viewmodel.UserRepositoryViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

class GroupMembersFragment : Fragment() {

    lateinit var binding: FragmentGroupMembersBinding
    private lateinit var userDataViewModel: UserRepositoryViewModel
    private val args: GroupMembersFragmentArgs by navArgs()
    private lateinit var adapter: GroupMemberAdapter

    var name: String? = null
    var id: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGroupMembersBinding.inflate(inflater, container, false)
        userDataViewModel = ViewModelProvider(this)[UserRepositoryViewModel::class.java]

        val simpleCallback = setUpCallback()
        adapter = GroupMemberAdapter(mutableListOf())

        userDataViewModel.getUsersById(args.group.members).observe(this, {
            adapter.addList(it as MutableList<User>)
            binding.memberRecyclerView.adapter = adapter
        })

        if (args.group.admins.contains(Firebase.auth.currentUser!!.uid)) {
            val itemTouchHelper = ItemTouchHelper(simpleCallback)
            itemTouchHelper.attachToRecyclerView(binding.memberRecyclerView)
        }

        binding.addMemberButton.setOnClickListener {
            val bs = BottomSheetDialogContact()
            val bundle = Bundle()
            bundle.putParcelable("group", args.group)
            bs.arguments = bundle
            bs.show(childFragmentManager, "Add")
        }

        return binding.root
    }

    private fun setUpCallback(): ItemTouchHelper.SimpleCallback {

        val simpleCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val user = adapter.getItem(position)
                name = user.firstName + " " + user.lastName
                id = user.uid
                if (Firebase.auth.currentUser!!.uid == id) {
                    Toast.makeText(
                        this@GroupMembersFragment.context,
                        "If you wish to leave the group go to the group page.",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    createDialog(position).show()
                }
                adapter.notifyItemChanged(position)
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                RecyclerViewSwipeDecorator.Builder(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
                    .addSwipeLeftBackgroundColor(
                        ContextCompat.getColor(
                            viewHolder.itemView.context, R.color.red_normal
                        )
                    )
                    .addSwipeLeftActionIcon(R.drawable.leave_icon)
                    .create()
                    .decorate()

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )

            }
        }
        return simpleCallback
    }

    @SuppressLint("SetTextI18n")
    private fun createDialog(position: Int): Dialog {
        val dialog = this.context?.let { Dialog(it) }!!
        dialog.setContentView(R.layout.invitation_dialog)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.attributes?.windowAnimations = R.style.animationDialog
        dialog.setCancelable(false)

        dialog.findViewById<Button>(R.id.btn_invite).text = "Remove"
        dialog.findViewById<TextView>(R.id.textViewInvite).text =
            "Do you want to remove $name from group?"
        dialog.findViewById<ImageView>(R.id.dialogImage).setImageResource(R.drawable.back_icon)


        dialog.findViewById<Button>(R.id.btn_invite).setOnClickListener {
            id?.let { it1 -> userDataViewModel.leaveGroup(it1, args.group.id!!) }
            adapter.remove(position)
            dialog.dismiss()
        }

        dialog.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            dialog.dismiss()
        }
        return dialog
    }


}