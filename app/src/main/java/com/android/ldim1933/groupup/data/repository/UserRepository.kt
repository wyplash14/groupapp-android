package com.android.ldim1933.groupup.data.repository

import android.app.Application
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.R
import com.android.ldim1933.groupup.common.Constants.GROUPS
import com.android.ldim1933.groupup.common.Constants.NOTIFICATIONS
import com.android.ldim1933.groupup.common.Constants.POLLS
import com.android.ldim1933.groupup.common.Constants.TASKS
import com.android.ldim1933.groupup.common.Constants.USERS
import com.android.ldim1933.groupup.data.model.NotificationData
import com.android.ldim1933.groupup.data.model.Poll
import com.android.ldim1933.groupup.data.model.Task
import com.android.ldim1933.groupup.data.model.User
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.util.*
import kotlin.collections.ArrayList


class UserRepository(application: Application) {
    private var mApplication: Application = application

    private val database =
        Firebase.database("https://groupup-6ed7f-default-rtdb.europe-west1.firebasedatabase.app/")
    private val mData = database.getReference(USERS)
    private val gData = database.getReference(GROUPS)
    private val tData = database.getReference(TASKS)
    private val pData = database.getReference(POLLS)
    private val nData = database.getReference(NOTIFICATIONS)
    private val imageUploader = ImageUploadRepository(mData)
    private var groupArrayList = ArrayList<String>()
    var groupDataList = MutableLiveData<ArrayList<String>>()
    var userData = MutableLiveData<User>()
    var userDataTelephoneList = MutableLiveData<ArrayList<User>>()
    private val userTelephoneArrayList = ArrayList<User>()

    fun create(user: User, image: Uri?) {

        user.uid?.let { it ->
            mData.child(it).setValue(user).addOnSuccessListener {
                Toast.makeText(
                    mApplication,
                    mApplication.getString(R.string.userdata),
                    Toast.LENGTH_LONG
                ).show()
                if (image != null) {
                    imageUploader.uploadPicture(user.uid!!, image)
                }
            }.addOnFailureListener { e ->
                Toast.makeText(mApplication, e.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    fun getUserbyId(userId: String) {
        mData.child(userId).get().addOnSuccessListener {
            val user = it.getValue(User::class.java)
            if (user != null) {
                userData.postValue(user!!)
            }
        }
    }

    fun getUsersById(userIdList: ArrayList<String>, memberDataList: MutableLiveData<List<User>>) {
        val tempList = ArrayList<User>()
        userIdList.forEach { userid ->
            mData.child(userid).get().addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result!!.exists()) {
                        val user = it.result.getValue(User::class.java)
                        if (user != null) {
                            tempList.add(user)
                        }
                        memberDataList.postValue(tempList)
                    }
                }
            }
            tempList.clear()
        }
    }

    fun getUserByTelephoneNumber(telephoneNumber: String) {
        mData.orderByChild("telephoneNumber").equalTo(telephoneNumber)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(it: DataSnapshot) {
                    it.children.forEach { child ->
                        val user: HashMap<String, String> = child.value as HashMap<String, String>
                        val uid = user["uid"]
                        val firstname = user["firstName"]
                        val lastname = user["lastName"]
                        val tel = user["telephoneNumber"]
                        val profilePictureUrl = user["profilePictureUrl"]
                        val token = user["token"]

                        userTelephoneArrayList.add(
                            User(
                                uid = uid,
                                firstName = firstname,
                                lastName = lastname,
                                telephoneNumber = tel,
                                profilePictureUrl = profilePictureUrl,
                                token = token
                            )
                        )

                        userDataTelephoneList.postValue(userTelephoneArrayList)

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.e("ERROR TELEPHONE NUMBER", "Error occurred during loading a specific user")
                }
            })
    }


    fun loadGroupsOfUser(userId: String, groupData: MutableLiveData<ArrayList<String>>) {
        mData.child(userId).child("groups").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (groupId in snapshot.children) {
                        groupArrayList.add(groupId.value.toString())
                    }
                    val l = ArrayList<String>()
                    l.addAll(groupArrayList)
                    groupData.postValue(l)
                }
                groupArrayList.clear()
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("CANCELLED GROUP OF USER", error.message)
            }

        })
    }

    fun addGroupToUser(userId: String, groupId: String) {

        mData.child(userId).child("groups").get().addOnSuccessListener {
            if (it.value == null) {
                mData.child(userId).child("groups").child("0").setValue(groupId)
            } else {
                val groups: ArrayList<String> = it.value as ArrayList<String>
                if (!groups.contains(groupId)) {
                    groups.add(groupId)
                }
                mData.child(userId).child("groups").setValue(groups).addOnSuccessListener {
                    Toast.makeText(
                        mApplication,
                        mApplication.getString(R.string.groupupdated),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }.addOnFailureListener { e ->
            Toast.makeText(mApplication, e.message, Toast.LENGTH_LONG).show()
        }
    }

    fun updateToken(uid: String, token: String) {
        mData.child(uid).child("token").setValue(token).addOnFailureListener {
            Toast.makeText(mApplication, mApplication.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
        }
    }

    fun leaveGroup(uid: String, groupId: String) {
        mData.child(uid).child("groups").get().addOnSuccessListener {
            val groupsList: ArrayList<String> = it.value as ArrayList<String>
            Log.i("GroupList", groupsList.toString())
            groupsList.remove(groupId)

            mData.child(uid).child("groups").setValue(groupsList).addOnFailureListener {
                Toast.makeText(mApplication, mApplication.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }
        }

        gData.child(groupId).child("members").get().addOnSuccessListener {
            val members: ArrayList<String> = it.value as ArrayList<String>
            members.remove(uid)
            gData.child(groupId).child("members").setValue(members).addOnFailureListener {
                Toast.makeText(mApplication, mApplication.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }
        }

        gData.child(groupId).child("admins").get().addOnSuccessListener {
            val members: ArrayList<String> = it.value as ArrayList<String>
            Log.i("ADMINS", members.toString())
            members.remove(uid)
            gData.child(groupId).child("admins").setValue(members).addOnFailureListener {
                Toast.makeText(mApplication, mApplication.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
            }
        }

        tData.get().addOnSuccessListener {
            it.children.forEach {
                it.children.forEach { node ->
                    val task = node.getValue(Task::class.java)
                    if (task!!.groupId == groupId) {
                        if (task.assignedById == uid || task.assignedToId == uid) {
                            node.ref.removeValue()
                        }
                    }
                }
            }
        }

        pData.orderByChild("groupId").equalTo(groupId).get().addOnSuccessListener { data ->
            data.children.forEach {
                val poll = it.getValue(Poll::class.java)
                poll!!.pollPoint.forEach {
                    it.voters.removeAll(Collections.singleton(uid));
                }

                it.ref.setValue(poll)
                if (poll.creatorId == uid) {
                    Log.i("It", it.ref.toString())
                    it.ref.removeValue()
                }

            }
        }

        nData.get().addOnSuccessListener {
            it.children.forEach {
                it.children.forEach { node ->
                    val not = node.getValue(NotificationData::class.java)
                    if (not!!.groupId == groupId) {
                        if (not.assignedById == uid || not.assignedToId == uid) {
                            node.ref.removeValue()
                        }
                    }
                }
            }
        }
    }
}
