package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.common.TaskStateEnum
import com.android.ldim1933.groupup.data.model.Task
import com.android.ldim1933.groupup.data.repository.TaskRepository

class TasksViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = TaskRepository(application)
    private val _tasksLiveData = MutableLiveData<List<Task>>()
    private var taskLiveData: MutableLiveData<List<Task>> = _tasksLiveData

    fun create(task: Task) {
        repository.create(task)
    }

    fun getUserTasks(uid: String, progressBar: ProgressBar? = null): LiveData<List<Task>> {
        repository.getUserTasks(uid, _tasksLiveData)
        if (progressBar != null) {
            progressBar.visibility = View.GONE
        }
        return taskLiveData
    }

    fun getGivenTasks(uid: String): LiveData<List<Task>> {
        repository.getGivenTasks(uid, _tasksLiveData)
        return taskLiveData
    }

    fun changeTaskState(uid: String, taskId: String, state: TaskStateEnum) {
        repository.changeTaskState(uid, taskId, state.title)
    }

    fun getGroupTasks(groupId: String): LiveData<List<Task>> {
        repository.getGroupTasks(groupId, _tasksLiveData)
        return taskLiveData
    }
}