package com.android.ldim1933.groupup.viewmodel

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.ldim1933.groupup.data.model.NotificationData
import com.android.ldim1933.groupup.data.repository.NotificationRepository

class NotificationViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = NotificationRepository(application)
    private val _notificationLiveData = MutableLiveData<List<NotificationData>>()
    private var notificationsLiveData: LiveData<List<NotificationData>> = _notificationLiveData

    fun create(notificationData: NotificationData) {
        repository.create(notificationData)
    }

    fun getUserNotifications(
        uid: String,
        progressBar: ProgressBar? = null
    ): LiveData<List<NotificationData>> {
        repository.getUserNotifications(uid, _notificationLiveData)
        if (progressBar != null) {
            progressBar.visibility = View.GONE
        }
        return notificationsLiveData
    }

    fun getGivenNotification(uid: String): LiveData<List<NotificationData>> {
        repository.getGivenNotifications(uid, _notificationLiveData)
        return notificationsLiveData
    }

}